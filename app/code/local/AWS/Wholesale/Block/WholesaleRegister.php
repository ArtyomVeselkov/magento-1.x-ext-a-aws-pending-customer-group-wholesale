<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

class AWS_Wholesale_Block_WholesaleRegister extends Mage_Customer_Block_Form_Register
{
    const CACHE_GROUP = 'non-cacheable-stuff';

    function _beforeToHtml()
    {
        return parent::_beforeToHtml();
    }
}
<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

class AWS_Wholesale_Block_Widget_Form extends Mage_Core_Block_Html_Link implements Mage_Widget_Block_Interface
{
    const MAGE_EVENT_NAME_RENDER_FORM_BEFORE = 'aws_wholesale_block_render_before';
    const MAGE_EVENT_NAME_RENDER_FORM_AFTER = 'aws_wholesale_block_render_after';

    protected $_collectedSections = array();
    protected $_scenario = null;
    protected $_showSchema = null;

    public function getScenario()
    {
        if (is_null($this->_scenario) || is_null($this->_showSchema)) {
            $this->initScenario();
        }
        return $this->_scenario;
    }

    public function getShowSchema()
    {
        if (is_null($this->_scenario) || is_null($this->_showSchema)) {
            $this->initScenario();
        }
        return $this->_showSchema;
    }

    private function initScenario()
    {
        $rule = $this->getTargetRule(false);
        $showFirstly = true;
        $this->_scenario = AWS_Wholesale_Helper_LogicHub::getExpressScenario($rule, $showFirstly);
        $this->_showSchema = $this->_scenario['show_elements'];
    }

    private function collectConfiguration()
    {
        $customer = AWS_Wholesale_Helper_Data::getCustomerData();
        $interests = array();
        $interestsGroup = AWS_Wholesale_Helper_Data::getInterestsGroups($interests);
        $preferences = AWS_Wholesale_Helper_Data::getAdditionalConfigurations();
        $preferences = array_merge(
            $preferences,
            array(
                'customer' => $customer,
                'required_fields' => $this->getRequiredFields(),
                'interests_groups' => $interestsGroup
            )
        );
        if (is_array($interests)) {
            foreach ($interests as $interestGroup => $interestData) {
                if (is_array($interestData)) {
                    $preferences['interests_list_' . $interestGroup] = $interestData;
                }
            }
        }
        return $preferences;
    }

    private static function collectCmsShortcut($content)
    {
        $result = array();
        $transit = preg_replace_callback('/{([^\}\|]+)(\|[^\}\|]*)?(\|[^\}\|]+)?}/', function ($fields) use (&$result) {
            $buffer = false;
            try {
                $buffer = array(
                    'element' => $fields[1],
                    'class' => ltrim($fields[2], '|'),
                    'rewrite' => ltrim($fields[3], '|'),
                    '__id' => trim($fields[0], '{}')
                );
            } catch (Exception $exception) {}
            if ($buffer) {
                $result[] = $buffer;
            }
        }, $content);
        return $result;
    }

    private function renderCmsBlock($cmsId)
    {
        $cmsContent = AWS_Wholesale_Helper_Data::renderCmsBlock($cmsId);
        return $cmsContent;
    }

    private function analyseCmsBlock($cmsContent)
    {
        $result = array();
        if (is_string($cmsContent)) {
            $result = static::collectCmsShortcut($cmsContent);
        }
        return $result;
    }

    private function processElements($elements, $configuration)
    {
        $showSchema = $this->getShowSchema();
        $elementsProcessor = new AWS_Wholesale_Helper_Elements($configuration);
        $htmlCollection = array();
        $requiredFields = is_array($configuration['required_fields']) ? $configuration['required_fields'] : array();
        if ($elementsProcessor) {
            foreach ($elements as $element) {
                try {
                    $elementName = $element['element'];
                    $class = $element['class'];
                    $rewriteString = $element['rewrite'];
                    $shortcutId = $element['__id'];
                    $disabledFlag = AWS_Wholesale_Helper_LogicHub::gdi($elementName, $showSchema);
                    $basicData = array(
                        'class' => $class,
                        'rewrite' => $rewriteString,
                        'disabled' => $disabledFlag ? '' : ' disabled="disabled"'
                    );
                    // disable validation (required fields) for non-required fields
                    if (!in_array($elementName, $requiredFields) && !stripos($class,'required-entry')) {
                        $basicData['validate'] = '';
                    }
                    if (in_array($elementName, $requiredFields) && !stripos($class,'required-entry')) {
                        $basicData['class'] .= ' required-entry';
                    }
                    if (method_exists($elementsProcessor, $elementName)) {
                        $htmlCollection[$shortcutId] = $elementsProcessor->{$elementName}($basicData);
                    }
                } catch (Exception $exception) {
                    $htmlCollection[$shortcutId] = '<!-- error occurred while rendering element (' . $elementName . ') -->';
                }
            }
        }
        return $htmlCollection;
    }

    private function collectData($cmsBlock)
    {
        $configuration = $this->collectConfiguration();
        $elements = $this->analyseCmsBlock($cmsBlock);
        $renderedElements = $this->processElements($elements, $configuration);
        return $renderedElements;
    }

    public function renderForm()
    {
        $cmsId = $this->getCmsBlockId();
        Mage::dispatchEvent(self::MAGE_EVENT_NAME_RENDER_FORM_BEFORE,
            array(
                'widget' => $this,
                'cms_block_id' => $cmsId
            )
        );
        $cmsBlock = $this->renderCmsBlock($cmsId);
        $renderedElements = $this->collectData($cmsBlock);
        $html = AWS_Wholesale_Helper_Data::replaceStringByMask($cmsBlock, $renderedElements, '{', '}');
        Mage::dispatchEvent(self::MAGE_EVENT_NAME_RENDER_FORM_AFTER,
            array(
                'widget' => $this,
                'html' => $html
            )
        );
        return $html;
    }

    public function getCmsBlockId()
    {
        return $this->getData('cms_block_id');
    }

    public function getRequiredFields($explodeFlag = true, $encryptFlag = false)
    {
        $data = $this->getData('required_fields');
        $data = $explodeFlag ? @explode(',', $data) : $data;
        if ($encryptFlag && is_string($data)) {
            $data = AWS_Wholesale_Helper_Data::encryptData($data);
        }
        return $data;
    }

    public function getSuccessUrl()
    {
        return $this->getUrl('customer/account/index');
    }

    public function getErrorUrl()
    {
        return $this->helper('core/url')->getCurrentUrl();
    }

    public function getPostActionUrl()
    {
        return $this->getUrl('customer/account/createPost');
    }

    public function getTargetRule($encryptFlag = false)
    {
        $target = $this->getData('target_registration_rule');
        if ($encryptFlag) {
            $target = AWS_Wholesale_Helper_Data::encryptData($target);
        }
        return $target;
    }
}

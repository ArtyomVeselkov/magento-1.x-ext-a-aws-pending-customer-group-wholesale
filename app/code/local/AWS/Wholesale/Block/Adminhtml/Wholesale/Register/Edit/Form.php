<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

class AWS_Wholesale_Block_Adminhtml_Wholesale_Register_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        /** @var AWS_Wholesale_Model_Register $register */
        $register = Mage::registry('aws_wholesale_edit_register');

        $targetGroupId = $register->getTargetGroupId();
        $targetGroupName = $register->getTargetGroupName();
        $actualCustomerGroup = $register->getActualCustomerGroup();
        $activeFlag = $targetGroupId === $actualCustomerGroup;

        $form = new Varien_Data_Form(
            array(
                'id'      => 'edit_form',
                'action'  => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
                'method'  => 'post',
                'enctype' => 'multipart/form-data'
            )
        );
        $form->setUseContainer(true);
        $this->setForm($form);

        $form->addField(
            'notes',
            'note',
            array(
                'text' => '<ul class="messages"><li class="notice-msg"><ul><li>'
                    .  Mage::helper('aws_wholesale')->__('Pending Registration Data')
                    . '</li></ul></li></ul>',
            )
        );

        $fieldset = $form->addFieldset(
            'aws_wholesale_register_fieldset',
            array(
                'legend' => Mage::helper('aws_wholesale')->__('General Information'),
            )
        );

        $fieldset->addField('id',
            'hidden', 
            array(
                'name' => 'id',
                'value' => $register->getId()
            )
        );


        $fieldset->addField(
            'actual_customer_group',
            'select',
            array(
                'label'    => Mage::helper('aws_wholesale')->__('Customer\'s Actual Group'),
                'title'    => Mage::helper('aws_wholesale')->__('Customer\'s Actual Group'),
                'class'    => 'required-entry',
                'required' => true,
                'name'     => 'customer_group_id',
                'value'    => $actualCustomerGroup,
                'values'   => AWS_Wholesale_Helper_Data::getCustomersGroupArray(),
                'after_element_html' => ' ' . Mage::helper('adminhtml')->__(
                        "Set to <i>'%s'</i> for activation. Currently account is <b style=\"color: %s;\">%sactive</b>.",
                        $targetGroupName,
                        $activeFlag ? 'green' : 'red',
                        $activeFlag ? '' : 'in'
                    ),
            )
        );

        $fieldset->addField(
            'notify_customer',
            'checkbox',
            array(
                'label'    => Mage::helper('aws_wholesale')->__('Notify Customer About This Change'),
                'title'    => Mage::helper('aws_wholesale')->__('Notify Customer About This Change'),
                'name'     => 'notify_customer',
                'checked'    => AWS_Wholesale_Helper_Data::getWholesaleConfigSample('aws_wholesale_activation/aws_wholesale_activation_enabled')
            )
        );

        $fieldset->addType('html_renderer', 'Aws_MetaBase_Block_Adminhtml_Renderer_Html');

        $fieldset->addField(
            'summary',
            'html_renderer',
            array(
                'name'     => 'description',
                'label'    => Mage::helper('aws_wholesale')->__('Summary On Activation Moment'),
                'title'    => Mage::helper('aws_wholesale')->__('Summary On Activation Moment'),
                'required' => false,
                'disabled' => true,
                'html_format' => '2',
                'value'    => $register->getSummaryFormatHtml2()
            )
        );

        $fieldset->addField(
            'target_group_id',
            'select',
            array(
                'label'    => Mage::helper('aws_wholesale')->__('Target Group (Activation)'),
                'title'    => Mage::helper('aws_wholesale')->__('Target Group (Activation)'),
                'class'    => 'required-entry',
                'required' => true,
                'name'     => 'target_group_id',
                'value'    => $targetGroupId,
                'values'   => AWS_Wholesale_Helper_Data::getCustomersGroupArray()
            )
        );

        $form->getElement('summary')->setDisabled('true')->setRequired(false);
        return parent::_prepareForm();
    }
}
<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

class AWS_Wholesale_Block_Adminhtml_Wholesale_Register_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('aws_wholesale_grid');
        $this->setDefaultSort('increment_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('aws_wholesale/register_collection');
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }

    protected function _prepareColumns()
    {
        $helper = Mage::helper('aws_wholesale');

        $this->addColumn('id', array(
            'header' => $helper->__('#'),
            'index'  => 'id'
        ));

        $this->addColumn('customer_id', array(
            'header' => $helper->__('Customer #'),
            'index'  => 'customer_id'
        ));
        
        $this->addColumn('customer_firstname', array(
            'header' => $helper->__('First Name'),
            'type'   => 'text',
            'index'  => 'first_name'
        ));

        $this->addColumn('customer_lastname', array(
            'header' => $helper->__('Last Name'),
            'type'   => 'text',
            'index'  => 'last_name'
        ));

        $this->addColumn('business_name', array(
            'header' => $helper->__('Business'),
            'type'   => 'text',
            'index'  => 'business_name'
        ));

        $this->addColumn('email', array(
            'header' => $helper->__('Email'),
            'type'   => 'text',
            'index'  => 'email'
        ));

        $groups = Mage::getResourceModel('customer/group_collection')
            ->addFieldToFilter('customer_group_id', array('gt'=> 0))
            ->load()
            ->toOptionHash();

        $this->addColumn('target_group_id', array(
            'header' => $helper->__('Target Group'),
            'index'  => 'target_group_id',
            'type'      =>  'options',
            'options'   =>  $groups,
        ));

        $renderData = array(
            array(
                'value' => '0',
                'type' => 'eq',
                'image' => 'aws/metabase/images/renderer/grid/minus-1-24px.png'
            ),
            array(
                'value' => '1',
                'type' => 'eq',
                'image' => 'aws/metabase/images/renderer/grid/plus-1-24px.png'
            )
        );
        $this->addColumn('activation_state', array(
            'header' => $helper->__('Activation State'),
            'type'   => 'options',
            'index'  => 'activation_state',
            'options' => array('Inactive', 'Active'),
            Aws_MetaBase_Block_Adminhtml_Renderer_Grid_IconRenderer::SELF_DATA_KEY => $renderData,
            'renderer' => 'aws_metabase/adminhtml_renderer_grid_iconRenderer',
        ));

        $this->addColumn('creation_time', array(
            'header' => $helper->__('Created On'),
            'index'  => 'creation_time',
            'type'   => 'datetime',
            'align'  => 'center',
            'width'  => '160'
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}
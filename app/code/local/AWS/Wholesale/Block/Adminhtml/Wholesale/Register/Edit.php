<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

class AWS_Wholesale_Block_Adminhtml_Wholesale_Register_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct(); 
        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_wholesale_register';
        $this->_blockGroup = 'aws_wholesale';
        $this->_updateButton('save', 'label', Mage::helper('aws_wholesale')->__('Save Registration'));
        $this->_removeButton('delete');
        $this->_removeButton('reset');
    }

    public function getHeaderText()
    {
        if (Mage::registry('aws_wholesale_edit_register') && Mage::registry('aws_wholesale_edit_register')->getId() ) {
            return Mage::helper('aws_wholesale')->__('Edit Registration');
        } else {
            return Mage::helper('aws_wholesale')->__('Add Registration');
        }
    }

    public function getHeaderCssClass()
    {
        return 'icon-head head-cms-page';
    }
}
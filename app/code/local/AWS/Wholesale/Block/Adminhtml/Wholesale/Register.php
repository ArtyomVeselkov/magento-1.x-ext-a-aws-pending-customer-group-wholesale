<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

class AWS_Wholesale_Block_Adminhtml_Wholesale_Register extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'aws_wholesale';
        $this->_controller = 'adminhtml_wholesale_register';
        $this->_headerText = Mage::helper('aws_wholesale')->__('Registry Of Pending Registration Requests');

        parent::__construct();
        $this->_removeButton('add');
    }
}
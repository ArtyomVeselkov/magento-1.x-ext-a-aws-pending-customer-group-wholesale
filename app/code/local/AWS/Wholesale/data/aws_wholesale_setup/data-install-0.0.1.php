<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

$additionalConfigs = array(
    'aws_wholesale/general/salt_password' => Mage::helper('core')->getRandomString(7),
    'aws_wholesale/general/client_emails' => 'test@aws3.com',
    'aws_wholesale/scenario_new_guest/scenario_new_guest_after_reg_email_flag' => '0,1',
    'aws_wholesale/scenario_regular_customer/scenario_regular_customer_after_reg_email_flag' => '0,1',
    'aws_wholesale/scenario_requested_customer/scenario_requested_customer_after_reg_email_send_email_flag' => '0,1',
    'aws_wholesale/aws_wholesale_interests/list' => 'a:6:{s:18:"_1494131939967_967";a:3:{s:11:"description";s:40:"This is the first item in list \'group_1\'";s:5:"label";s:12:"First option";s:5:"group";s:7:"group_1";}s:18:"_1494131962738_738";a:3:{s:11:"description";s:41:"This is the second item in list \'group_1\'";s:5:"label";s:13:"Second option";s:5:"group";s:7:"group_1";}s:18:"_1494131976927_927";a:3:{s:11:"description";s:40:"This is the third item in list \'group_1\'";s:5:"label";s:12:"Third option";s:5:"group";s:7:"group_1";}s:17:"_1494131992093_93";a:3:{s:11:"description";s:38:"This is a first item in list \'group_2\'";s:5:"label";s:23:"Another list - option 1";s:5:"group";s:7:"group_2";}s:18:"_1494132046469_469";a:3:{s:11:"description";s:41:"This is the second item in list \'group_2\'";s:5:"label";s:23:"Another list - option 2";s:5:"group";s:7:"group_2";}s:18:"_1494132056526_526";a:3:{s:11:"description";s:40:"This is the third item in list \'group_2\'";s:5:"label";s:23:"Another list - option 3";s:5:"group";s:7:"group_2";}}',
    'aws_wholesale/general/manage_rules' => 'a:1:{s:18:"_1494035147943_943";a:4:{s:11:"description";s:25:"Demo Pending Registration";s:9:"rule_name";s:17:"default_wholesale";s:12:"referrer_url";s:10:"/wholesale";s:21:"target_customer_group";s:1:"2";}}',
    'aws_wholesale/general/additional_settings' => 'a:9:{s:18:"_1494131039107_107";a:3:{s:11:"description";s:18:"Recaptcha Site Key";s:3:"key";s:18:"recaptcha_site_key";s:5:"value";s:0:"";}s:18:"_1494131059649_649";a:3:{s:11:"description";s:18:"\'image\' or \'audio\'";s:3:"key";s:14:"recaptcha_type";s:5:"value";s:5:"image";}s:18:"_1494131062626_626";a:3:{s:11:"description";s:17:"\'dark\' or \'light\'";s:3:"key";s:15:"recaptcha_theme";s:5:"value";s:5:"light";}s:18:"_1494131065940_940";a:3:{s:11:"description";s:21:"\'compact\' or \'normal\'";s:3:"key";s:14:"recaptcha_size";s:5:"value";s:6:"normal";}s:18:"_1494131069602_602";a:3:{s:11:"description";s:31:"0 - disable, 1 - enable logging";s:3:"key";s:10:"debug_mode";s:5:"value";s:1:"0";}s:18:"_1494131077329_329";a:3:{s:11:"description";s:63:"Attribute of customer\'s entity with type \'File\' for saving file";s:3:"key";s:23:"customer_attribute_file";s:5:"value";s:0:"";}s:17:"_1494131086063_63";a:3:{s:11:"description";s:79:"Attribute of customer\'s entity with type \'varchar\' or \'text\' for saving website";s:3:"key";s:26:"customer_attribute_website";s:5:"value";s:0:"";}s:18:"_1494131090947_947";a:3:{s:11:"description";s:79:"Attribute of customer\'s entity with type \'varchar\' or \'text\' for saving comment";s:3:"key";s:26:"customer_attribute_comment";s:5:"value";s:0:"";}s:18:"_1494194984383_383";a:3:{s:11:"description";s:84:"Attribute of customer\'s entity with type \'varchar\' or \'interests\' for saving comment";s:3:"key";s:28:"customer_attribute_interests";s:5:"value";s:0:"";}}'
);

$blocksHeaderFooter = array(
    'header' => '{{template config_path="design/email/header"}}
{{inlinecss file="email-inline.css"}} 

',
    'footer' => '{{template config_path="design/email/footer"}} 
    
'
);

$blocks = array(
    'scenario_guest_used_email_warning_show' => array(
        'title' => 'Notification message for guest requesting registration quote with already used email',
        'save_to_config' => 'aws_wholesale/scenario_guest_used_email/scenario_guest_used_email_warning_show'
    ),
    'scenario_wholesale_warning_show' => array(
        'title' => 'Notification message for registered customer accessing registration page',
        'save_to_config' => 'aws_wholesale/scenario_wholesale/scenario_wholesale_warning_show'
    ),
    'generic_registration_form' => array(
        'title' => 'Pending Registration Form',
        'save_to_config' => 'aws_wholesale/general/non_cachable_blocks'
    )
);

$emails = array(
    'aws_wholesale_request_customer' => array(
        'title' => 'AWS New Pending Request - To Customer',
        'subject' => '{{var store.getFrontendName()}} - Registration Request',
        'styles' => '',
        'save_to_config' => array(
            'aws_wholesale/scenario_new_guest/scenario_new_guest_after_reg_email',
            'aws_wholesale/scenario_regular_customer/scenario_regular_customer_after_reg_email',
            'aws_wholesale/scenario_requested_customer/scenario_requested_customer_after_reg_email'
        )
    ),
    'aws_wholesale_request_client' => array(
        'title' => 'AWS New Pending Request - To Client',
        'subject' => '{{var store.getFrontendName()}} - Registration Request',
        'styles' => '',
        'save_to_config' => array(
            'aws_wholesale/scenario_new_guest/scenario_new_guest_after_reg_email_client',
            'aws_wholesale/scenario_regular_customer/scenario_regular_customer_after_reg_email_client',
            'aws_wholesale/scenario_requested_customer/scenario_regular_customer_after_reg_email_client'
        )
    ),
    'aws_wholesale_activation_email' => array(
        'title' => 'AWS Pending Request - Activation (Customer)',
        'subject' => '{{var store.getFrontendName()}} - Activation',
        'styles' => '',
        'save_to_config' => 'aws_wholesale/aws_wholesale_activation/aws_wholesale_activation_on_email'
    ),
    'aws_wholesale_deactivation_email' => array(
        'title' => 'AWS Pending Request - Deactivation (Customer)',
        'subject' => '{{var store.getFrontendName()}} - Deactivation',
        'styles' => '',
        'save_to_config' => 'aws_wholesale/aws_wholesale_activation/aws_wholesale_activation_off_email'
    )
);

function saveConfig($path, $value)
{
    if (is_array($path)) {
        foreach ($path as $singlePath) {
            saveConfig($singlePath, $value);
        }
    } elseif (is_string($path) && strlen($path) && 2 === substr_count($path, '/')) {
        Mage::getConfig()->saveConfig($path, $value, 'default', 0);
    }
}

foreach ($blocks as $blockName => $blockItem) {
    $block = Mage::getModel('cms/block');
    $block->load($blockName);
    if (!$block->getId()) {
        $templateContent = @AWS_Wholesale_Helper_Data::loadExtensionStatic($blockName . '.html');
        if (!$templateContent) {
            $templateContent = sprintf('<p>No template file with code `%s` was found.</p>', $blockName);
        }
        $blockItem['content'] = $templateContent;
        $block->setTitle($blockItem['title'])
            ->setIdentifier($blockName)
            ->setStores(array(0))
            ->setIsActive(1)
            ->setContent($blockItem['content']);
        $block->save();
        if (isset($blockItem['save_to_config'])) {
            saveConfig($blockItem['save_to_config'], $block->getId());
        }
    }
}

foreach ($emails as $emailCode => $emailItem) {
    $template = Mage::getModel('adminhtml/email_template');
    $template->loadByCode($emailCode);
    if (!$template->getId()) {
        $template->loadDefault($emailCode);
    }
    if (!$template->getId()) {
        $templateContent = @AWS_Wholesale_Helper_Data::loadExtensionStatic($emailCode . '.html');
        if (!$templateContent) {
            $templateContent = sprintf('<p>No template file with code `%s` was found.</p>', $emailCode);
        }
        $emailItem['text'] = $templateContent;
        $template->setTemplateSubject($emailItem['subject'])
            ->setTemplateCode($emailItem['title'])
            ->setTemplateText($blocksHeaderFooter['header'] . $emailItem['text'] . $blocksHeaderFooter['footer'])
            ->setTemplateStyles($emailItem['styles'])
            ->setModifiedAt(Mage::getSingleton('core/date')->gmtDate())
            ->setAddedAt(Mage::getSingleton('core/date')->gmtDate())
            ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);
        try {
            $template->save();
        if (isset($emailItem['save_to_config'])) {
            saveConfig($emailItem['save_to_config'], $template->getId());
        }
        } catch (Exception $exception) {
            // we suppose that those templates already exist
        }
    }
}

foreach ($additionalConfigs as $configPath => $configValue) {
    saveConfig($configPath, $configValue);
}

$installer->endSetup();
AWS Customers Pending Group Registration (formally "AWS Wholesale")
===================================================================

# 1. Intro
  This extension implements pending (delayed) customers registration in order to obtain `Customer Group` differ from
   regular `General` group (which every customer get after successful registration process).
   
  Furthermore, extension is also provided with custom form builder based on using "shortcuts", so end-user can
   build it's own designed forms, for each `Target Group`.
   
  All submitted registration forms are collected in the admin panel page `Registry Of Pending Registration Requests`
   and can be easily managed (activated or deactivated). 
   
   *Grid for managing pending and activated requests.*
   ![Main Grid](images/intro_grid_1.png)
   
   *Main view page for activating/deactivating registration submissions.*
   ![Edit Registration Page](images/intro_edit_page_1.png)

## 1.1 Key Functionality:
   1. Grid with all requested pending registrations.
   2. Create as many pending registration groups, as it is needed.
   3. Customer and site owner are automatically notified on each form submission or activation state changes, 
    email templates are configurable via Magento Admin Panel.
   4. Easy customizable registration form: use "shortcuts" to define where to place each field ("First Name", 
    "Country", "Password", "Comment" etc) and configure it's via parameters for each form element (e.g. required field,
    data validation etc).
   5. No limitations in amount of custom registration forms: everything is hold on via Widgets (registration form 
    insertion) and CMS Blocks (form markup and design).
   6. Configure additional checkboxes list for questioning customer (via `interest` field).
   7. Recaptcha support.
     
     
## 1.2 Notes:
   1. Extension is working with `Register` object which is a request data for obtaining account with special 
    `Customer Group`. 
   2. After submitting registration form customer becomes registered (if he wasn't) and from that moment till 
    his registration request will be accepted, all data is kept in-sync (`Register` and `Customer Account`).
    After registration request is accepted data shown on the `Edit Registration` page corresponds with customer profile
    on the moment of registration acceptance and would not be changed (until `Register` becomes inactive).
   3. Available elements for building registration form:
      - First and Last Name
      - Email
      - Password
      - Company (Business Name)
      - Country
      - Region (State/Province)
      - City
      - Postcode
      - Street (multi-lines, depends on current Magento configuration)
      - Phone Number
      - Website Link
      - Comment (textarea)
      - Interests (checkboxes list)
      - Recaptcha
      - [ File ] is experimental and requires additional `Customer Attribute` for saving files.
   4. Each element can be configured via parameters to change it look-and-feel.
   
   
# 2. How To Start
  1. Install two extensions: `AWS_Wholsale` and `Aws_MetaBase`.
  
  2. New new menu item should appear:
     
     ![New menu item](images/howto_manu_item_1.png)
     
  3. Navigate to the `Absolute > Custsomer Pending Group > Settings`
     
     ![Settings page](images/howto_settings_whole_1.png)
     
  4. Firstly specify:
  
     1. `Client Email Identity To Send From`
     
     2. `Client Emails For Receiving Emails` - several emails can be specified with comma
     
     3. Create new or change existed rule under `Manage Rules` section:
         listed here rules will determine which groups will require delayed manual acceptation before customer can 
         obtain specified `Customer Group`. `Description` columns are not used by extension, they only for usage 
         comfortable.
         
        (Note: `Referrer URL` helps to determine target customer group on form submission, but as form already confines
          encrypted target rule identifier, so this field can be left empty, it is present here for advanced usage)
     4. Then specify needed scenario's settings or left default if everything is ok.
     
     5. Then navigate to `CMS Block > Pending Registration Form (generic_registration_form)` as an example of form
      markup. Change it according to your needs. Notes about form building process listed in next chapter.
      
      ![Generic Form (HTML) :: CMS Block](images/howto_generic_cms_block_html_1.png)
     6. Next create new `CMS Page`, for example *'Wholesale'*.
      After specifying `Page Information` data, go to the `Content` tab and click on the `Insert Widget` item on editor 
      panel. You should see next popup window on which we need find `Widget Type` named `Aws Pending Registration Form`.   
      
      ![CMS :: Insert PRF Widget](images/howto_cms_widget_prf_inset_1.png)
      Here You can:
      - select target rule (which was configured on step **3** in `Manage Rules` section);
      - select which form elements are requires (keep in mind, that if some form element is marked as "required", but 
       is not present in the form markup, than processing with registration would be imposable);
      - select CMS Block which will be used as markup for the form.
      Then press `Insert Widget` and save current CMS Page.
      
      7. Now navigate to the newly created page and try to full fill the form.
      
       ![Ready Form - Example 1](images/howto_ready_form_example_1.png)
       
       ![Ready Form - Example 2](images/howto_ready_form_example_2.png)
       
      8. After submitting form:
         - ff form submitter was a guest than regular customer account is created.
           **(!)** If no password field is present on the form than password will be generated automatically (and such
           password is accessible in the email template via variable `passwordautogenerated`);
         - 2 emails will be send - one for client and second to the customer (if so configured in the Admin Panel);
         - if some error occurred - then page will be refreshed with warning message and full field data (except 
          password if it was provided).
         
      9. If form was successfully submitted than request can be accepted in the Admin Panel in 
       `Registry Of Pending Registration Requests`. For this change `` to specified early in the settings (or follow 
       the note provided right after the `Customer Group` select-box).
       ![Accept Request Example](images/howoto_customer_accept_request_1.png)
      
# 3. Form Builder
## 3.1 General
   All process of creating custom form is splitted into 3 stages:
      
   1. Create Markup (CMS Block) with form HTML caracas where each form element is placed via corresponded "shortcuts";
   2. Create CMS Page (or anything else where widget can be placed) for Magento public page;
   3. Configure and insert Widget `Aws Pending Registration Form` onto CMS Page (if You use it).
 
## 3.2 Modify Markup
   1. *Table of "shortcuts":*
   
   | Form Field               	| Shortcut  	|
   |--------------------------	|-----------	|
   | First Name               	| firstname 	|
   | Last Name                	| lastname  	|
   | Email                    	| email     	|
   | Password (two fields)    	| password  	|
   | Company                  	| company   	|
   | Country                  	| country   	|
   | Region                   	| region    	|
   | City                     	| city      	|
   | Postcode                 	| zip       	|
   | Street                   	| street    	|
   | Phone Number             	| phone     	|
   | Website Link             	| website   	|
   | Comment                  	| comment   	|
   | Interests                	| interest  	|
   | Recaptcha                	| recaptcha 	|
   | Submit                   	| submit    	|
   | File (* is experimental) 	| file      	|
   
   2. Each of this shortcut is rendered via it's own template (`/static/element/*.html`) and can be configured / extend 
    via additional parameters.
   
   | Parameter        	| Description                                                                                                                                                                             	|
   |------------------	|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	|
   | placeholder      	| Placeholder for the field [AUTO GENERATED]                                                                                                                                              	|
   | title            	| Will asign title to the element [AUTO GENERATED]                                                                                                                                        	|
   | class            	| Will append additional CSS classes to the rendered element                                                                                                                               	|
   | style            	| Additional CSS style                                                                                                                                                                    	|   
   | max_length       	| [TEXT FIELDS ONLY] Maximal letters which can be written into the field                                                                                                                  	|
   | disabled         	| [NOT RECOMMENDED TO SPECIFY EXPLICITLY!] Used to make fields disabled, must be in format like (  disabled="disabled"  ) [AUTO GENERATED]                                                	|
   | id               	| [NOT RECOMMENDED TO SPECIFY EXPLICITLY!] Will replace ID attribute of the HTML element [AUTO GENERATED]                                                                                 	|
   | name             	| [NOT RECOMMENDED TO SPECIFY EXPLICITLY!] Will replace NAME attribute of the HTML element [AUTO GENERATED]                                                                               	|
   | validate         	| [NOT RECOMMENDED TO SPECIFY EXPLICITLY!] Magento specific CSS class for data validation [AUTO GENERATED]                                                                                	|
   | value            	| [NOT RECOMMENDED TO SPECIFY EXPLICITLY!] Will overwrite initial value of the element [AUTO GENERATED]                                                                                   	|
   | child_data       	| [NOT RECOMMENDED TO SPECIFY EXPLICITLY!] Advanced parameter for composite elements (like "Country") [AUTO GENERATED]                                                                    	|   
   | each_before_html 	| [AVAILABLE ONLY FOR STREET] Specify HTML markup that will be appended to each street line field BEFORE each street template, default is ( <div class="field"><div class="input-box">  ) 	|
   | each_after_html  	| [AVAILABLE ONLY FOR STREET] Specify HTML markup that will be appended to each street line field AFTER each street template, default is (  </div></div>  )                               	|        
   | default_value    	| Advanced usage for some composite elements  [AUTO GENERATED]                                                                                                                            	|

   There can be also created additional parameters (except current), for doing this should be created copeis of 
    redefined templates in `static/element/*.html` named `static/element/*-local.html` and new parameters like 
    like `{{new_parameter}}` placed in the HTML templates.
        
  3. Placing "shortcuts" with or without parameters.
     - General structrute of shortcut is: `{shortuct_name|additional_css_classes|other_additional_parameters}`.
        Third part `other_additional_parameters` must be in any next form:
         - parameter1=value1
         - parameter1=value1,parameter2=value2 with spaces
         - parameter1=value1,"parameter2=value2 with spaces"
         - "parameter1=value1","parameter2=value2","parameter3=value3"
         - parameter1=value with "quotes" here,parameter=and quotes "here"
        
     - simple "shortcut" usage:
       
       *For example place "First Name" and "Last Name" fields:* 
       ```html
       Your First Name: {firstname}
       Your Last Name: {lastname}
       ```
       
     - advanced with specifying additional CSS classes:
     
       *For example place "submit button":*
        
       Example 1:
       ```html
       Now please submit your request {submit|submit-style-button}      
        ```
       Example 2:
       ```html
       Now please submit your request {submit|submit-style-button green-button}      
       ```
     
     - advanced with specifying additional CSS classes and additional parameters:
     
       *For example place "submit button" and Recaptcha:*
               
       ```html
           <div class="submit-button-wrapper" style="margin: 0 auto; text-align: center;">
               {submit|submit-style|"title=Submit Request","style=margin: 10px auto; display: block;"}
               {recaptcha||"style=position: relative; display: inline-block;"}
           </div>    
       ```
       In this example in case of `recaptcha` "shortcut" extra CSS classes were missed that's why we 
        left `{recaptcha||...}` empty section among two `|` symbols.

        
# 4. Dev Notes
  There is several moments for developers:
  
  1. So `Register` is a request for obtaining account with custom `Customer Group`, changing group is doing manually.
  2. We perform changes in `Register` only by firing observer `customer_save_after` (but other observers are also used).
  3. There are 5 + 1 + 3 scenarios
     - 5 - are on the `/<page_with_registration_from_url>` page
     - 1 - some error occurred on the `/page_with_registration_from_url` page
     - 3 - one on saving data from `Registry Of Pending Registration Requests` page in Admin panel,
             second is a save from `My Account`, third - from `Manage Customer` in Admin Panel.
 4. During installation process are created several elements:
    - email templates
    - CMS blocks
    - table `aws_wholesale_register`
    - `core_config_data` entries
 5. For installing CMS Blocks, email templates and loading form elements markups are used static content files (HTML), 
  which are located in the `/static/` directory of this extension.
 6. **(!)** If you need to change markup of some `form element` - there is no need to make changes directly in the, 
  for example, `/static/element/region.html` file. You can create copy of it with name `region-local.html` and then 
  extension will use this `region-local.html` instead of `region.html`.  
 7. Extension also depends on some functionality from `Aws_MetaBase` (in `community` code pool), so for normal working 
   `Aws_MetaBase` is required. 
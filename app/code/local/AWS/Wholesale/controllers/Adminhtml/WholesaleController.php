<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

class AWS_Wholesale_Adminhtml_WholesaleController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->_title($this->__('Wholesale'))->_title($this->__('Main Page'));
        if ($this->getRequest()->getQuery('ajax')) {
            $this->_forward('grid');
            return;
        }

        $this->loadLayout();
        $this->_setActiveMenu('aws_wholesale/aws_wholesale');
        $this->_addContent($this->getLayout()->createBlock('aws_wholesale/adminhtml_wholesale_register'));
        $this->renderLayout();
    }

    public function editAction()
    {
        $registerId = Mage::app()->getRequest()->getParam('id');
        $register = Mage::getModel('aws_wholesale/register')->load($registerId);
        
        if ($registerId  || $registerId == 0) {
            
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data))
                $register->setData($data);
            
            Mage::register('aws_wholesale_edit_register', $register);
            
            $this->loadLayout();
            $this->_setActiveMenu('aws_wholesale');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Wholesale'), Mage::helper('adminhtml')->__('Wholesale'));
            
            $this->_addContent($this->getLayout()->createBlock('aws_wholesale/adminhtml_wholesale_register_edit'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }
    
    public function newAction()
    {
        $this->_forward('edit');
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $register = Mage::getModel('aws_wholesale/register');
            $id = @$data['id'];
            $customerGroupId = @$data['customer_group_id'];
            $forceNotify = isset($data['notify_customer']) ? true : false;
            if ('' != $id && AWS_Wholesale_Helper_Data::testCustomerGroupId($customerGroupId)) {
                $register = $register->load($data['id']);
            }
            $targetCustomerGroupId = @$data['target_group_id'];
            if (
                !is_null($targetCustomerGroupId) && false !== $targetCustomerGroupId &&
                $register->getId() && $register->getData('target_group_id') !== $targetCustomerGroupId) {
                $register->setData('target_group_id', $targetCustomerGroupId
            );
                $register->save();
            }
            if ($register->getId()) {
                /** @var Mage_Customer_Model_Customer $customer */
                $customer = $register->getCorrespondedCustomer();
                if ($customer->getId()) {
                    $customer->setData('group_id', $customerGroupId);
                    AWS_Wholesale_Model_Register::$dataTunnel['force_notify_customer'] = $forceNotify;
                    $customer->save();
                }
            }
            // we don't need actually to save it here, because of observer on "onCustomerSave"
            // $register->save();
        }
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $system = Mage::getModel('aws_wholesale/register')->load($data['register_id']);
            $system->delete();
        }
        $this->_redirect('*/*/');
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('customer/aws_wholesale');
    }

    public function gridAction()
    {
            $this->loadLayout();
            $this->_setActiveMenu('aws_wholesale/aws_wholesale');
            $this->_addContent($this->getLayout()->createBlock('aws_wholesale/adminhtml_wholesale_register'));
            $this->renderLayout();
    }

}
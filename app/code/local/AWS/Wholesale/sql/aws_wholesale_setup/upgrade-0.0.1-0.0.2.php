<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$tableName = $installer->getTable('aws_wholesale/register');

if (!$installer->getConnection()->tableColumnExists($tableName, 'target_group_id')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('aws_wholesale/register'),
        'target_group_id',
        "SMALLINT(5) UNSIGNED NOT NULL COMMENT 'Target customer group for registration'"
    );
}

$installer->endSetup();

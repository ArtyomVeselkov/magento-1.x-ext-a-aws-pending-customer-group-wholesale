<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

class AWS_Wholesale_Model_Resource_Register extends Mage_Core_Model_Resource_Db_Abstract
{
    protected static $importMap = array(
        'id' => 'customer_id',
        'email' => 'email',
        'group_id' => 'customer_group'
    );

    public function _construct()
    {
        $this->_init('aws_wholesale/register', 'id');
    }

    public function loadByEmail($register, $email)
    {
        $result = false;
        $adapter = $this->_getReadAdapter();
        $bind    = array('email' => $email);
        $select  = $adapter->select()
            ->from($this->getMainTable(), array($this->getIdFieldName()))
            ->where('email = :email');
        $registerId = $adapter->fetchOne($select, $bind);
        if ($registerId) {
            $this->load($register, $registerId);
            $result = $register->getId() ? true : false;
        } else {
            $register->setData(array());
        }
        return $result;
    }

    public function loadByCustomerId($register, $customerId)
    {
        $result = false;
        $adapter = $this->_getReadAdapter();
        $bind    = array('customer_id' => $customerId);
        $select  = $adapter->select()
            ->from($this->getMainTable(), array($this->getIdFieldName()))
            ->where('customer_id = :customer_id');
        $registerId = $adapter->fetchOne($select, $bind);
        if ($registerId) {
            $this->load($register, $registerId);
            $result = $register->getId() ? true : false;
        } else {
            $register->setData(array());
        }
        return $result;
    }

    public function _beforeSave($object)
    {
        $buffer = $object->getData('interests_default');
        // $object->setData('interests_default', Zend_Serializer::serialize($buffer));
        return parent::_beforeSave($object);
    }

    public function _afterLoad($object)
    {
        parent::_afterLoad($object);
        $buffer = $object->getData('interests_default');
        // $object->setData('interests_default', Zend_Serializer::unserialize($buffer));
        return $this;
    }
}
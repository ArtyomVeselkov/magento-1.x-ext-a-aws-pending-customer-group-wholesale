<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

class AWS_Wholesale_Model_Resource_Register_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('aws_wholesale/register');
    }
    
    public function registerExistsByCustomerId($customerId)
    {
        $backup = clone ($this->getSelect());
        $this->getSelect()->reset();
        $this->addFieldToFilter('customer_id', $customerId);
        $item = $this->fetchItem();
        if ($item && $item->getId()) {
            $result = true;
        } else {
            $result =false;
        }
        $select = $this->getSelect();
        $select = $backup;
    }

}
<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

class AWS_Wholesale_Model_Register extends Mage_Core_Model_Abstract
{
    const CUSTOMER_GROUP_NOT_LOGGED_IN = 'NOT LOGGED IN';
    protected $_targetGroupName = null;

    public static $fieldsDescription = array(
        'first_name' => 'First Name',
        'last_name' => 'Last Name',
        'email' => 'Email',
        'phone_primary' => 'Telephone',
        'customer_id' => 'Customer #',
        'customer_group' => 'Group #',
        'website_link' => 'Website',
        'interests_default' => 'Interests',
        'comment' => 'Comment',
        'country_text' => 'Country',
        'state_text' => 'Region',
        'city_text' => 'City',
        'address' => 'Street',
        'zip_code' => 'Postcode',
        'business_name' => 'Business Name'
    );

    public static $dataTunnel = array(
        'requested' => false,
        'register' => null,
        'scenario' => AWS_Wholesale_Helper_LogicHub::LH_R_ERROR,
        'data' => null,
        'save_file' => false,
        'force_notify_customer' => null,
        'save_billing' => false,
        'save_other_fields' => false,
        'password_auto_generated' => false,
        'rule' => null
    );

    public static $importMap = array(
        'first_name' => 'firstname',
        'last_name' => 'lastname',
        'customer_id' => 'entity_id',
        'customer_group' => 'group_id',
        'email' => 'email'
    );

    public static $importMapParams = array(
        'website_link' => 'website',
        'interests_default' => 'interest',
        'comment' => 'comment',
        'file' => 'file'
    );

    public static $importMapAddress = array(
        'address' => 'street',
        'zip_code' => 'postcode',
        'city_text' => 'city',
        'state_text' => 'region',
        'country_text' => 'country_id',
        'phone_primary' => 'telephone',
        'business_name' => 'company'
    );

    public static $updateMap = array(
        'customer_group' => 'group_id',
    );

    public function _construct()
    {
        $this->_init('aws_wholesale/register');
    }

    public static function getNameByGroupId($id = false)
    {
        $groupName = self::CUSTOMER_GROUP_NOT_LOGGED_IN;
        if (false !== $id) {
            try {
                /** @var Mage_Customer_Model_Group $group */
                $group = Mage::getModel('customer/group')->load($id);
            } catch (\Exception $exception) {
                return $groupName;
            } finally {
                $groupName = $group->getCustomerGroupCode();
            }
        }
        return $groupName;
    }

    public function getActualCustomerGroup($textual = false)
    {
        $groupName = self::CUSTOMER_GROUP_NOT_LOGGED_IN;
        $notLoggedIn = Mage::getModel('customer/group');
        $notLoggedIn->load($groupName, 'customer_group_code');
        $groupId = $notLoggedIn->getId();


        $customerId = $this->getCustomerId();
        if (!$customerId || 0 > $customerId) {
            return $textual ? $groupName : $groupId;
        }
        /** @var Mage_Customer_Model_Customer $customer */
        $customer = Mage::getModel('customer/customer')->load($customerId);
        if ($customer->getId()) {
            $groupId = $customer->getGroupId();
            $groupName = static::getNameByGroupId($groupId);
        }
        return $textual ? $groupName : $groupId;
    }

    public function loadByEmail($customerEmail)
    {
        if (!strlen($customerEmail)) {
            return false;
        }
        return @$this->_getResource()->loadByEmail($this, $customerEmail);
    }

    public function loadByCustomerId($customerId)
    {
        if (!$customerId) {
            return false;
        }
        return @$this->_getResource()->loadByCustomerId($this, $customerId);
    }

    public function getSummary()
    {
        $result = '';
        foreach (static::$fieldsDescription as $field => $label) {
            $value = $this->getData($field);
            if (is_array($value)) {
                $value = implode("\r\n\t\t\t", $value);
            }
            $result .= sprintf(
                "%s\r\n\t\t\t%s\r\n",
                $label, $value
            );
        }
        return $result;
    }

    public function _beforeSave()
    {
        if (is_null($this->getCreationTime())) {
            $this->setCreationTime(time());
        }
        return parent::_beforeSave();
    }

    public function formatFields($field, $value)
    {
        switch ($field) {
            case 'customer_id':
                $value = sprintf(
                    '<a target="_blank" href="%s" alt="Go to customer page">View Customer #%s</a>',
                    Mage::helper('adminhtml')->getUrl('adminhtml/customer/edit/index/id', array('id' => $value)),
                    $value
                );
                break;
            case 'email':
                $value = sprintf(
                    '<a href="mailto:%s" alt="Email this customer">%s</a>',
                    $value,
                    $value
                );
                break;
            case 'customer_group':
                try {
                    $group = Mage::getModel('customer/group');
                    $group->load($value);
                    $groupName = false;
                    if ($group->getId()) {
                        $groupName = $group->getCode();
                    }
                    if ($groupName) {
                        $value = sprintf('%s (Group ID: %s)', $groupName, $value);
                    } else {
                        $value = sprintf('Group ID: %s', $value);
                    }
                } catch (Exception $exception) {
                }
                break;
            case 'website_link':
                if (is_string($value) && strlen($value) && false !== filter_var($value, FILTER_VALIDATE_URL)) {
                    $value = sprintf(
                        '<a target="_blank" href="%s" alt="Visit customer site">%s</a>',
                        $value,
                        $value
                    );
                }
                break;
            default:
                break;
        }
        return $value;
    }

    public function getSummaryFormatHtml2()
    {
        $result = array(
            'header' => 'Profile Snapshot (on time of acceptance)',
            'content' => array(
                'table' => array()
            )
        );
        foreach (static::$fieldsDescription as $field => $label) {
            $value = $this->getData($field);
            $value = $this->formatFields($field, $value);
            @$result['content']['table']['td1'][] = '<b>' . $label . '</b>';
            @$result['content']['table']['td2'][] = '<pre>' . $value . '</pre>';
        }
        return $result;
    }

    public static function checkByCustomerId($customerId)
    {
        $customerId = @(int)$customerId;
        // we suppress customer with ID = 0
        if ($customerId <= 0) {
            return false;
        }
        /** @var AWS_Wholesale_Model_Resource_Register_Collection $registerCollection */
        $registerCollection = Mage::getModel('aws_wholesale/register')->getCollection();
        $registerCollection->registerExistsByCustomerId($customerId);
    }

    public static function updateActivatedWholesale($register, $customer, $targetGroupId)
    {
        $updateMapChange = array_values(static::$updateMap);
        foreach (static::$updateMap as $toRegister => $fromCustomer) {
            $newValue = $customer->getData($fromCustomer);
            if (!is_null($newValue) && $register->getData($toRegister) != $newValue) {
                $updateMapChange[$fromCustomer] = true;
                $register->setData($toRegister, $newValue);
            }
        }
        return static::performActivationStateCheck($register, $updateMapChange, $targetGroupId);
    }

    public function afterLoad()
    {
        return parent::afterLoad();
    }

    public static function updateCustomerData($register, $customer, $targetGroupId, $data, $savedBillingId = false)
    {
        if (
            AWS_Wholesale_Model_Config_Source_ActivationState::ACTIVATION_STATE_ENABLED == $register->getActivationState()
            // || $register->getActualCustomerGroup() == $targetGroupId
        ) {
            return static::updateActivatedWholesale($register, $customer, $targetGroupId);
        } else {
            return static::importCustomerData($register, $customer, $targetGroupId, $data, $savedBillingId);
        }
    }

    /**
     * @param $register
     * @param $customer Mage_Customer_Model_Customer
     * @param $targetGroupId
     * @param $data
     * @return int
     */
    public static function importCustomerData($register, $customer, $targetGroupId, $data, $savedBillingId = false)
    {
        $deltaChange = array_merge_recursive(
            array_values(static::$importMap),
            array_values(static::$importMapParams),
            array_values(static::$importMapAddress)
        );
        $deltaChange = array_unique($deltaChange);
        foreach (static::$importMapParams as $toRegister => $fromCustomer) {
            $newValue = $data->getData($fromCustomer);
            if (!is_null($newValue) && $register->getData($toRegister) != $newValue) {
                $deltaChange[$fromCustomer] = true;
                $register->setData($toRegister, $newValue);
            }
        }
        foreach (array_merge_recursive(static::$importMap, static::$importMapParams) as $toRegister => $fromCustomer) {
            $newValue = $customer->getData($fromCustomer);
            if (!is_null($newValue) && $register->getData($toRegister) != $newValue) {
                $deltaChange[$fromCustomer] = true;
                $register->setData($toRegister, $newValue);
            }
        }
        $billing = AWS_Wholesale_Helper_Data::getBillingAddress($customer, $savedBillingId);
        foreach (static::$importMapAddress as $toRegister => $fromCustomer) {
            $newValue = $billing->getData($fromCustomer);
            if (!is_null($newValue) && $register->getData($toRegister) != $newValue) {
                $deltaChange[$fromCustomer] = true;
                $register->setData($toRegister, $newValue);
            }
        }
        return static::performActivationStateCheck($register, $deltaChange, $targetGroupId);
    }

    public static function performActivationStateCheck($register, $deltaChange, $targetGroupId)
    {
        $register->setData('target_group_id', $targetGroupId);
        $activationState = $register->getActivationState();
        $newActivationState = static::changeActivationState($activationState, $deltaChange, $targetGroupId, $register->getCustomerGroup());
        $register->setActivationState($newActivationState);
        $register->save();
        return ($newActivationState != $activationState) && !is_null($activationState) ? $newActivationState : AWS_Wholesale_Model_Config_Source_ActivationState::ACTIVATION_STATE_UNCHANGED;
    }

    public static function changeActivationState($state, $deltaChange, $targetGroupId, $newGroupId)
    {
        if ($targetGroupId == $newGroupId) {
            return AWS_Wholesale_Model_Config_Source_ActivationState::ACTIVATION_STATE_ENABLED;
        } else {
            return AWS_Wholesale_Model_Config_Source_ActivationState::ACTIVATION_STATE_PENDING;
        }
    }

    public function getCorrespondedCustomer()
    {
        $customerId = $this->getCustomerId();
        $customer = Mage::getModel('customer/customer');
        try {
            $customer->load($customerId);
        } catch (Exception $exception) {
            $customer = null;
        }
        return $customer;
    }

    public static function reverseCustomerDataSave($customer)
    {
        if (!$customer) {
            return ;
        }
        try {
            AWS_Wholesale_Helper_LogicHub::saveUpdatedCustomerData($customer);
            $customer->save();
        } catch (Exception $exception) {
            return false;
        } finally {
            return true;
        }
    }

    /**
     * Populate (duplicate) data with the same keys as in customer attributes
     */
    public function populateWithCustomerFieldAlias()
    {
        $deltaChange = array_merge_recursive(
            array_values(static::$importMap),
            array_values(static::$importMapParams),
            array_values(static::$importMapAddress)
        );
        $deltaChange = array_unique($deltaChange);
        foreach ($deltaChange  as $toRegister => $fromCustomer) {
            $value = $this->getData($toRegister);
            $this->setData($fromCustomer, $value);
        }
    }

    public function getTargetGroupName()
    {
        if (is_null($this->_targetGroupName)) {
            try {
                $group = Mage::getModel('customer/group');
                $group->load($this->getTargetGroupId());
                if ($group->getId()) {
                    $this->_targetGroupName = $group->getCode();
                }
            } catch (Exception $exception) {}
        }
        return $this->_targetGroupName;
    }
}
<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

class AWS_Wholesale_Model_Config_Source_Doc
{
    public static $_doc = null;

    public function getDoc()
    {
        if (is_null(static::$_doc)) {
            static::$_doc = AWS_Wholesale_Helper_Data::loadDocFile();
        }
        return array(
            array(
                'image_path' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'doc' . DIRECTORY_SEPARATOR,
                'doc' => static::$_doc
            )
        );
    }

    public function toOptionArray()
    {
        return static::getDoc();
    }
}
<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

class AWS_Wholesale_Model_Config_Source_Settings
{
    public static $rulesColumns = array(
        'key' => array(
            'label' => 'Parameter\'s Name',
            'type' => Aws_MetaBase_Model_Source_Config_ArrayColumns::COLUMN_TEXT,
            'style' => 'width:120px'
        ),
        'value' => array(
            'label' => 'Value',
            'type' => Aws_MetaBase_Model_Source_Config_ArrayColumns::COLUMN_TEXT,
            'style' => 'width:120px'
        )
    );


    public function toOptionArray()
    {
        return static::$rulesColumns;
    }
}
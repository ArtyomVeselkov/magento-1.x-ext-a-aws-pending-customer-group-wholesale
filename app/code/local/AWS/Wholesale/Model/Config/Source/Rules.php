<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

class AWS_Wholesale_Model_Config_Source_Rules
{
    public static $rulesColumns = array(
        'rule_name' => array(
            'label' => 'Rule Name',
            'type' => Aws_MetaBase_Model_Source_Config_ArrayColumns::COLUMN_TEXT,
            'style' => 'width:120px'
        ),
        'referrer_url' => array(
            'label' => 'Referrer URL (RegExp)',
            'type' => Aws_MetaBase_Model_Source_Config_ArrayColumns::COLUMN_TEXT,
            'style' => 'width:120px'
        ),
        'target_customer_group' => array(
            'label' => 'Target Customer Group',
            'type' => Aws_MetaBase_Model_Source_Config_ArrayColumns::COLUMN_SELECT,
            'style' => 'width:120px',
            'values' => array(
                array(
                    'label' => 'Not logged in',
                    'value' => 0
                ),
                array(
                    'label' => 'Registered',
                    'value' => 1
                )
            )
        )
    );

    protected static $_postProcessed = false;

    protected static function loadCustomersGroups()
    {
        try {
            $groupsSource = Mage::getSingleton('adminhtml/system_config_source_customer_group');
            $groups = $groupsSource->toOptionArray();
            static::$rulesColumns['target_customer_group']['values'] = $groups;
        } catch (Exception $exception) {}
    }


    public function toOptionArray()
    {
        if (!static::$_postProcessed) {
            static::loadCustomersGroups();
        }
        return static::$rulesColumns;
    }
}
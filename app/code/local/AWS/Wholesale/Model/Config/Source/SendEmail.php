<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

class AWS_Wholesale_Model_Config_Source_SendEmail
{
    const SEND_EMAIL_TO_CUSTOMER_LABEL = 'Customer';
    const SEND_EMAIL_TO_CUSTOMER = 0;
    const SEND_EMAIL_TO_CLIENT_LABEL = 'Client';
    const SEND_EMAIL_TO_CLIENT = 1;

    public static $states = array(
        array(
            'label' => self::SEND_EMAIL_TO_CUSTOMER_LABEL,
            'value' => self::SEND_EMAIL_TO_CUSTOMER
        ),
        array(
            'label' => self::SEND_EMAIL_TO_CLIENT_LABEL,
            'value' => self::SEND_EMAIL_TO_CLIENT
        )
    );

    public function toOptionArray()
    {
        return static::$states;
    }
}
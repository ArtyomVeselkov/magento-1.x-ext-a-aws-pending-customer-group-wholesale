<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

class AWS_Wholesale_Model_Config_Source_ActivationState
{
    const ACTIVATION_STATE_ENABLED_LABEL = 'enabled';
    const ACTIVATION_STATE_ENABLED = 1;
    const ACTIVATION_STATE_PENDING_LABEL = 'pending';
    const ACTIVATION_STATE_PENDING = 0;
    const ACTIVATION_STATE_UNCHANGED = -1;

    public static $states = array(
        array(
            'label' => self::ACTIVATION_STATE_ENABLED_LABEL,
            'value' => self::ACTIVATION_STATE_ENABLED
        ),
        array(
            'label' => self::ACTIVATION_STATE_PENDING_LABEL,
            'value' => self::ACTIVATION_STATE_PENDING
        )
    );

    public function toOptionArray()
    {
        return static::$states;
    }
}
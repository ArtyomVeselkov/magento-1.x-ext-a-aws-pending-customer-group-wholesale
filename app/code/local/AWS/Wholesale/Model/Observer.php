<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

class AWS_Wholesale_Model_Observer
{
    /** @var Mage_Customer_Model_Session|null  */
    protected $_session = null;
    protected $_controller = null;
    protected $_currentRule = 0;
    protected $_rules = array(
        [
            'rule_name' => 'Demo Group',
            'redirect_url' => '*/*/*',
            'referrer_url' => 'wholesale',
            'target_customer_group' => 1
        ]
    );
    protected static $_nonCachableBlocks = false;
    protected static $_sendEmails = array();
    protected static $_preformedUpdates = array();

    public function __construct()
    {
        if (!$this->_session) {
            $this->_rules = AWS_Wholesale_Helper_Data::getWholesaleConfigRules();
            $this->_session = Mage::getSingleton('customer/session');
            $this->_controller = @Mage::app()->getFrontController()->getAction();
        }
    }

    public function registerWholesaleQuote($observer)
    {
        AWS_Wholesale_Helper_Data::updateRegisterModelMaps();
        // 1. retrieve needed data
        $data = $observer->getData();
        $controller = is_array($data) ? $data['controller_action'] : null;
        $fullActionName = Mage::app()->getFrontController()->getAction()->getFullActionName();
        $pageReferrer = Mage::helper('core/http')->getHttpReferer();
        $requestedRule = $controller->getRequest()->getParam('rule');
        if (is_string($requestedRule) && strlen($requestedRule)) {
            $rule = AWS_Wholesale_Helper_Data::detectRuleByRequest($this->_rules, $requestedRule, true);
        } else {
            $rule = AWS_Wholesale_Helper_Data::detectRuleByUrl($this->_rules, $pageReferrer);
        }
        if (
            !is_a($controller, 'Mage_Customer_AccountController') ||
            !stristr('customer_account_createpost', $fullActionName) ||
            !is_array($rule)
        ) {
            return ;
        }
        $this->_controller = $controller;
        $requestData = new Varien_Object($controller->getRequest()->getParams());
        $register = Mage::getModel('aws_wholesale/register');
        /** @var Mage_Customer_Model_Customer $customer */
        $customer = $this->_session->getCustomer();

        // 2. determine scenario
        $conditionsFlag = AWS_Wholesale_Helper_LogicHub::determineConditions($requestData, $this->_session, $customer, $register, $rule);
        $scenario = AWS_Wholesale_Helper_LogicHub::figureOutScenario($conditionsFlag);

        // 3. validate data
        $dataSave = array();
        $validationResult = AWS_Wholesale_Helper_LogicHub::validateWholesaleData($requestData, $dataSave, $scenario, $customer);
        if (count($validationResult)) {
            $this->makeRedirectError($requestData, $validationResult, $scenario);
        }

        // 4. choose - decline, save only or make request
        //    if guest -- create account
        //    if registered -- update customer's data
        AWS_Wholesale_Model_Register::$dataTunnel['scenario'] = $scenario;
        AWS_Wholesale_Model_Register::$dataTunnel['register'] = $register;
        AWS_Wholesale_Model_Register::$dataTunnel['data'] = $dataSave;
        AWS_Wholesale_Model_Register::$dataTunnel['rule'] = $rule;
        switch ($scenario['allow_registration']) {
            case AWS_Wholesale_Helper_LogicHub::LH_A_REGISTRATION_WITH_REQUEST :
                // 1. create account
                // 2. create register
                // 3. send email
                AWS_Wholesale_Helper_LogicHub::setWholesaleSpecificData($dataSave, $customer);
                AWS_Wholesale_Model_Register::$dataTunnel['requested'] = true;
                break;
            case AWS_Wholesale_Helper_LogicHub::LH_A_UPDATE_WITH_REQUEST :
                // 1. create register
                // 2. send email
                AWS_Wholesale_Model_Register::$dataTunnel['requested'] = true;
                AWS_Wholesale_Helper_LogicHub::setWholesaleSpecificData($dataSave, $customer);
                AWS_Wholesale_Model_Register::reverseCustomerDataSave($customer);
                break;
            case AWS_Wholesale_Helper_LogicHub::LH_A_UPDATE_DATA_ONLY :
                // 1. update customer and register
                AWS_Wholesale_Model_Register::$dataTunnel['requested'] = false;
                AWS_Wholesale_Helper_LogicHub::setWholesaleSpecificData($dataSave, $customer);
                AWS_Wholesale_Model_Register::reverseCustomerDataSave($customer);
                break;
            default:
            case AWS_Wholesale_Helper_LogicHub::LH_A_NOT_ALLOWED_UPDATE_DATA :
                // 1. redirect
                AWS_Wholesale_Model_Register::$dataTunnel['requested'] = false;
                $this->makeRedirectError($requestData, $validationResult, $scenario);
                break;
        }
    }

    public function makeRedirectSuccessful($scenario)
    {
        $message = AWS_Wholesale_Helper_LogicHub::getRedirectSuccessfulMessage($scenario);
        $url = $scenario['on_success_redirect'];
        if (strlen($message)) {
            Mage::getSingleton('core/session')->addSuccess($message);
        }
        if (strlen($url)) {
            static::performRedirect($url);
        }
    }

    public function makeRedirectError($data, $validationResult, $scenario)
    {
        $exception = AWS_Wholesale_Helper_LogicHub::prepareException($scenario, $validationResult);
        if (is_a($data, 'Varien_Object')) {
            $data = $data->getData();
        }
        if ($exception) {
            $this->_session->setCustomerFormData($data);
            Mage::getSingleton('core/session')->addException(
                $exception,
                AWS_Wholesale_Helper_LogicHub::prepareErrorMessage($scenario, $validationResult)
            );
        }
        static::performRedirect(@$scenario['on_error_redirect']);
    }

    protected static function performRedirect($url)
    {
        if (is_null($url)) {
            $redirectUrl = AWS_Wholesale_Helper_Data::getReferrerUrl();
        } else {
            $redirectUrl = Mage::getUrl($url, array('_secure' => true));
        }
        Mage::app()->getResponse()->setRedirect($redirectUrl)->sendResponse();
        exit;
    }

    public function adminhtmlCustomerSaveAfter($observer)
    {
        AWS_Wholesale_Helper_Data::updateRegisterModelMaps();
        $customer = $observer->getData('customer');
        $this->onSaveCustomer($customer);
    }

    public function frontendCustomerSaveAfter($observer)
    {
        AWS_Wholesale_Helper_Data::updateRegisterModelMaps();
        $customer = $observer->getData('customer');
        $this->onSaveCustomer($customer);
    }

    protected function onSaveCustomer($customer)
    {
        AWS_Wholesale_Helper_Data::updateRegisterModelMaps();
        $dataTunnel = AWS_Wholesale_Model_Register::$dataTunnel;
        if (!is_array($dataTunnel)) {
            return ;
        }
        $scenario = @$dataTunnel['scenario'];
        /** @var AWS_Wholesale_Model_Register $register */
        $register = @$dataTunnel['register'];
        $requested = @$dataTunnel['requested'];
        $data = new Varien_Object(@$dataTunnel['data']);
        if (!is_array($scenario) || !is_object($data) || !$register) {
            if (!AWS_Wholesale_Helper_LogicHub::checkOpenRequest($customer, $register, $scenario)) {
                return;
            }
        }
        $customerId = $customer->getId();
        $emailAction = false;
        $activationChange = AWS_Wholesale_Model_Config_Source_ActivationState::ACTIVATION_STATE_UNCHANGED;
        if (!$register || (($register->getCustomerId() !== $customerId) && $customerId) || !$register->getId()) {
            if (!$register) {
                $register = Mage::getModel('aws_wholesale/register');
            }
            $register->loadByCustomerId($customerId);
            if (!$register->getId()) {
                $register->loadByEmail($customer->getEmail());
            }
        }
        if ($register->getId()) {
            $targetGroupId = $register->getData('target_group_id');
        } elseif (is_array(@$dataTunnel['rule'])) {
            $targetGroupId = AWS_Wholesale_Helper_Data::getWholesaleGroupId(@$dataTunnel['rule']);
        } else {
            return ;
        }
        if (in_array($register->getId(), static::$_preformedUpdates)) {
            @$scenario['allow_registration'] = AWS_Wholesale_Helper_LogicHub::LH_A_UPDATE_DATA_ONLY;
        }

        switch (@$scenario['allow_registration']) {
            case AWS_Wholesale_Helper_LogicHub::LH_A_REGISTRATION_WITH_REQUEST:
                AWS_Wholesale_Helper_LogicHub::updateCustomerBillingAddress($customer, $data);
                $activationChange = AWS_Wholesale_Model_Register::updateCustomerData($register, $customer, $targetGroupId, $data);
                $emailAction = 'scenario_new_guest';

                break;
            case AWS_Wholesale_Helper_LogicHub::LH_A_UPDATE_DATA_ONLY:
                AWS_Wholesale_Model_Register::$dataTunnel['save_billing'] = true;
                @AWS_Wholesale_Model_Register::$dataTunnel['save_other_fields'] = true;
                AWS_Wholesale_Helper_LogicHub::updateCustomerBillingAddress($customer, $data);
                $activationChange = AWS_Wholesale_Model_Register::updateCustomerData($register, $customer, $targetGroupId, $data);
                $emailAction = '';
                break;
            case AWS_Wholesale_Helper_LogicHub::LH_A_UPDATE_WITH_REQUEST:
                $savedBillingId = AWS_Wholesale_Helper_LogicHub::updateCustomerBillingAddress($customer, $data);
                $activationChange = AWS_Wholesale_Model_Register::updateCustomerData($register, $customer, $targetGroupId, $data, $savedBillingId);
                $emailAction = 'scenario_regular_customer';
                break;
        }
        static::$_preformedUpdates[] = $register->getId();
        $dataForEmails = array(
            'customer' => $customer,
            'register' => $register,
            'customeractualgroupname' => $register->getActualCustomerGroup(true),
            'passwordautogenerated' => @$dataTunnel['password_auto_generated']
        );
        if (!in_array($register->getId(), static::$_sendEmails)) {
            AWS_Wholesale_Helper_Data::sendAllEmails($emailAction, $dataForEmails, $requested, $activationChange);
            static::$_sendEmails[] = $register->getId();
        }
    }

    public function frontendCustomerSaveBefore($observer) {
        AWS_Wholesale_Helper_Data::updateRegisterModelMaps();
        $customer = $observer->getCustomer();
        AWS_Wholesale_Helper_LogicHub::saveUpdatedCustomerData($customer);
    }

    public function preventCachingBlock($observer)
    {
        $block = $observer->getBlock();
        if ($block->getType() != 'cms/block') {
            return ;
        }
        $cmsBlockId = $block->getData('block_id');
        $cmsBlock = Mage::getModel('cms/block');
        $cmsBlock->load($cmsBlockId);
        if ($cmsBlock->getId()) {
            if (!is_array(static::$_nonCachableBlocks)) {
                static::$_nonCachableBlocks = AWS_Wholesale_Helper_Data::getNonCachableBlocks();
            }
            if (in_array($cmsBlock->getId(), static::$_nonCachableBlocks)) {
                $block->unsetData('cache_tags');
                $block->unsetData('cache_lifetime');
            }
        }
    }
}
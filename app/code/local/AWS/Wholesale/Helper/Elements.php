<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

class AWS_Wholesale_Helper_Elements extends Varien_Object
{
    protected $_renderMethods = array();
    protected $_renderTemplates = array();
    protected $_sessionForm = array();
    protected $_customer;
    protected $_billingAddress;
    protected $_session;
    protected $_register;

    public function __construct($data)
    {
        parent::__construct();
        $this->addData($data);
        $this->_introspectElements();
        $this->_initTemplates();
        $this->_initCustomer();
    }

    protected function _getConfig($parameter, $default = '')
    {
        if ($this->hasData($parameter)) {
            return $this->getData($parameter);
        } else {
            return $default;
        }
    }

    protected function _introspectElements()
    {
        $classReflection = new ReflectionClass($this);
        $renderMethods = array();
        foreach ($classReflection->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
            if ($method->class === $classReflection->getName()) {
                $renderMethods[] = $method->name;
            }
        }
        $this->_renderMethods = $renderMethods;
    }

    protected function _initTemplates()
    {
        $templates = array();
        $content = '';
        if (is_array($this->_renderMethods)) {
            foreach ($this->_renderMethods as $methodName) {
                // safe overwrite of template in case of `elementname-local.html`
                $content = @AWS_Wholesale_Helper_Data::loadExtensionStatic($methodName . '-local.html', 'element');
                if ($content) {
                    $templates[$methodName] = $content;
                } elseif($content = @AWS_Wholesale_Helper_Data::loadExtensionStatic($methodName . '.html', 'element')) {
                    $templates[$methodName] = $content;
                } else {
                    try {
                        unset($this->_renderMethods[$methodName]);
                    } catch (Exception $exception) {}
                }
            }
        }
        $this->_renderTemplates = $templates;
    }

    protected function _initCustomer()
    {
        $customer = $this->getCustomer();
        if ($customer && is_a($customer, 'Mage_Customer_Model_Customer')) {
            $this->_customer = $customer;
        } else {
            $this->_customer = Mage::getModel('customer/customer');
        }
        $this->_billingAddress =  $this->_customer->getPrimaryBillingAddress() ?
            $this->_customer->getPrimaryBillingAddress() : Mage::getModel('customer/address');
        $this->_session = Mage::getSingleton('customer/session');
        if (is_array($this->_session->getCustomerFormData()) && count($this->_session->getCustomerFormData())) {
            $this->_sessionForm = $this->_session->getCustomerFormData();
        }
        $register = $this->getRegister();
        if ($register && is_a($register, 'AWS_Wholesale_Model_Register')) {
            $this->_register = $register;
        }
    }

    protected function _processRewrites($rewrites)
    {
        $result = [];
        if (is_string($rewrites) && strlen($rewrites)) {
            $resultRaw = @str_getcsv($rewrites);
            if (count($resultRaw)) {
                array_map(function ($item) use (&$result) {
                    $buffer = explode('=', $item, 2);
                    if (2 == count($buffer)) {
                        $result[$buffer[0]] = $buffer[1];
                    }
                }, $resultRaw);
            }
        }
        return $result;
    }

    protected function _processElement($elementData, $elementName)
    {
        // 1. prepare configuration object
        $result = [];
        if (is_array($elementData)) {
            $result = new Varien_Object();
            $result->setData($elementData);
        } elseif (!is_a($elementData, 'Varien_Object')) {
            return false;
        } else /*if (is_a($elementData, 'Varien_Object'))*/ {
            $result = $elementData;
        }
        if ($result->hasData('merge')) {
            $merge = $result->getData('merge');
            foreach ($merge as $key => $value) {
                if (!is_null($value) && is_string($key)) {
                    $result->setData($key, $value);
                }
            }
            $result->unsetData('merge');
        }
        // 2. full fill object with default data
        $rewrites = $this->_processRewrites($result->getRewrite());
        $defaults = array(
            'render_method_name' => $elementName,
            'id' => $elementName,
            'name' => $elementName,
            'title' => $elementName,
            'max_length' => 255,
            'validate' => '',
            'class' => '',
            'placeholder' => '',
            'value' => '',
            'child_data' => '',
            'disabled' => '',
            'default_value' => ''
        );
        $configuration = array_merge($defaults, $result->getData(), $rewrites);
        $result->setData($configuration);
        return $result;
    }

    protected function _render($configuration)
    {
        $result = false;
        if (is_a($configuration, 'Varien_Object')) {
            $renderMethodName = $configuration->getData('render_method_name');
            $configurationData = $configuration->getData();
            if (isset($this->_renderTemplates[$renderMethodName])) {
                $result = AWS_Wholesale_Helper_Data::replaceStringByMask(
                    $this->_renderTemplates[$renderMethodName],
                        $configurationData
                );
            }
        }
        return $result;
    }

    protected function _getCustomerData($field, $default = '')
    {
        if (is_array($this->_sessionForm) && isset($this->_sessionForm[$field])) {
            $result = $this->_sessionForm[$field];
        } elseif ($this->_customer->hasData($field)) {
            $result = $this->_customer->getData($field);
        } elseif ($this->_billingAddress->hasData($field)) {
            $result = $this->_billingAddress->getData($field);
        } elseif ($this->_register && $this->_register->hasData('')) {
            $result = $this->_register->getData($field);
        } else {
            $result = $default;
        }
        return $result;
    }

    protected function _escapeHtml($data, $allowedTags = null)
    {
        return Mage::helper('core')->escapeHtml($data, $allowedTags);
    }

    protected function _renderCountryBlock($defValue = null, $name = 'country_id', $id = 'country', $title = 'Country', $class = '', $extra = '')
    {
        $html = '';
        try {
            if (is_null($defValue)) {
                $defValue = $this->getCountryId();
            }
            $cacheKey = 'DIRECTORY_COUNTRY_SELECT_STORE_AWS_WHOLESALE_BLOCK_RENDER' . Mage::app()->getStore()->getCode();
            if (Mage::app()->useCache('config') && $cache = Mage::app()->loadCache($cacheKey)) {
                $options = unserialize($cache);
            } else {
                $collection = Mage::getModel('directory/country')->getResourceCollection()->loadByStore();
                $options = $collection->toOptionArray();
                if (Mage::app()->useCache('config')) {
                    Mage::app()->saveCache(serialize($options), $cacheKey, array('config'));
                }
            }
            $html = Mage::app()->getLayout()->createBlock('core/html_select')
                ->setName($name)
                ->setId($id)
                ->setTitle(Mage::helper('directory')->__($title))
                ->setClass('validate-select ' . $class)
                ->setValue($defValue)
                ->setOptions($options)
                ->setExtraParams($extra)
                ->getHtml();
        } catch (Exception $exception) {}
        return $html;
    }

    public function firstname($data)
    {
        $selfData = [
            'title' => 'First Name',
            'placeholder' => 'First Name',
            'merge' => $data,
            'value' => $this->_getCustomerData('firstname')
        ];
        $configuration = $this->_processElement($selfData, __FUNCTION__);
        $html = $this->_render($configuration);
        return $html;
    }

    public function lastname($data)
    {
        $selfData = [
            'title' => 'Last Name',
            'placeholder' => 'Last Name',
            'merge' => $data,
            'value' => $this->_getCustomerData('lastname')
        ];
        $configuration = $this->_processElement($selfData, __FUNCTION__);
        $html = $this->_render($configuration);
        return $html;
    }

    public function company($data)
    {
        $bussinessCustomerAttribute = $this->_getConfig('customer_attribute_businessname', 'businessname');
        $validateClass = Mage::helper('customer/address')->getAttributeValidationClass('company');
        $selfData = [
            'title' => 'Business Name',
            'placeholder' => 'Business Name',
            'merge' => $data,
            'value' => $this->_getCustomerData($bussinessCustomerAttribute),
            'validate' => $validateClass
        ];
        $configuration = $this->_processElement($selfData, __FUNCTION__);
        $html = $this->_render($configuration);
        return $html;
    }

    public function street($data)
    {
        $validateClass = Mage::helper('customer/address')->getAttributeValidationClass('street');
        $value = $this->_getCustomerData('street');
        $values = @explode("\n", $value);
        $valueFirst = $this->_escapeHtml($values[0]);
        $selfData = [
            'title' => 'Street',
            'placeholder' => 'Street',
            'merge' => $data,
            'value' => $valueFirst,
            'validate' => $validateClass,
            'name' => __FUNCTION__ . '[]',
            'each_before_html' => '',
            'each_after_html' => ''
        ];
        $configuration = $this->_processElement($selfData, __FUNCTION__);
        // render additional address lines based on magento configuration
        $streetLines = Mage::helper('customer/address')->getStreetLines();
        $childData = '';
        for ($_i = 2, $_n = $streetLines; $_i <= $_n; $_i++) {
            $configurationN = clone $configuration;
            $configurationN->setData('title', $configurationN->getData('title') . ' ' . (string)$_i);
            $configurationN->setData('placeholder', $configurationN->getData('placeholder') .  ' ' . (string)$_i);
            $configurationN->setData('id', $configurationN->getData('id') . (string)$_i);
            // disable next lines also if first is disabled
            if (stripos($configuration->getData('disabled'), 'disabled')) {
                $configurationN->setData('disabled_n', $configuration->getData('disabled'));
            }
            $configurationN->setData('disabled', $configurationN->getData('disabled_n'));
            $valueNext = isset($values[$_i]) ? $this->_escapeHtml($values[$_i]) : '';
            $configurationN->setData('value', $valueNext);
            $buffer = $this->_render($configurationN);
            $buffer = (string)$configuration->getData('each_before_html') . $buffer . (string)$configuration->getData('each_after_html');
            $childData .= PHP_EOL . $buffer;
            try {
                unset($configurationN);
            } catch (Exception $exception) {}
        }
        $buffer = $this->_render($configuration);
        $buffer = (string)$configuration->getData('each_before_html') . $buffer . (string)$configuration->getData('each_after_html');
        $html = $buffer . PHP_EOL . $childData;
        return $html;
    }

    public function city($data)
    {
        $validateClass = Mage::helper('customer/address')->getAttributeValidationClass('city');
        $selfData = [
            'title' => 'City',
            'placeholder' => 'City',
            'merge' => $data,
            'value' => $this->_getCustomerData('city'),
            'validate' => $validateClass
        ];
        $configuration = $this->_processElement($selfData, __FUNCTION__);
        $html = $this->_render($configuration);
        return $html;
    }

    public function region($data)
    {
        $validateClass = Mage::helper('customer/address')->getAttributeValidationClass('region');
        $selfData = [
            'title' => 'State/Province',
            'placeholder' => 'State/Province',
            'merge' => $data,
            'value' => $this->_getCustomerData('region'),
            'default_value' => $this->_getCustomerData('region_id'),
            'validate' => $validateClass
        ];
        $configuration = $this->_processElement($selfData, __FUNCTION__);
        $html = $this->_render($configuration);
        return $html;
    }

    public function zip($data)
    {
        $validateClass = Mage::helper('customer/address')->getAttributeValidationClass('postcode');
        $selfData = [
            'title' => 'Zip/Postal Code',
            'name' => 'postcode',
            'placeholder' => 'Zip/Postal Code',
            'merge' => $data,
            'value' => $this->_getCustomerData('postcode'),
            'validate' => $validateClass
        ];
        $configuration = $this->_processElement($selfData, __FUNCTION__);
        $html = $this->_render($configuration);
        return $html;
    }

    public function country($data)
    {
        $countryValueId = $this->_getCustomerData('country_id');
        $selfData = [
            'id' => 'country',
            'name' => 'country_id',
            'title' => 'Country',
            'placeholder' => 'Country',
            'merge' => $data,
            'value' => $countryValueId,
        ];
        $configuration = $this->_processElement($selfData, __FUNCTION__);
        $countryHtml = @$this->_renderCountryBlock(
            $countryValueId,
            $configuration->getData('name'),
            $configuration->getData('id'),
            $configuration->getData('title'),
            $configuration->getData('class'),
            $configuration->getData('disabled') . ' placeholder="' . $configuration->getData('placeholder') . '"'
        );
        $configuration->setData('child_data', $countryHtml);
        $html = $this->_render($configuration);
        return $html;
    }

    public function phone($data)
    {
        $validateClass = Mage::helper('customer/address')->getAttributeValidationClass('telephone');
        $selfData = [
            'title' => 'Phone Number',
            'placeholder' => 'Phone Number',
            'id' => 'telephone',
            'name' => 'telephone',
            'merge' => $data,
            'value' => $this->_getCustomerData('telephone'),
            'validate' => $validateClass
        ];
        $configuration = $this->_processElement($selfData, __FUNCTION__);
        $html = $this->_render($configuration);
        return $html;
    }

    public function email($data)
    {
        $selfData = [
            'title' => 'Email Address',
            'placeholder' => 'Email Address',
            'merge' => $data,
            'value' => $this->_getCustomerData('email')
        ];
        $configuration = $this->_processElement($selfData, __FUNCTION__);
        $html = $this->_render($configuration);
        return $html;
    }

    public function file($data)
    {
        $selfData = [
            'title' => 'Upload File',
            'placeholder' => 'Upload File',
            'merge' => $data
        ];
        $configuration = $this->_processElement($selfData, __FUNCTION__);
        $html = $this->_render($configuration);
        return $html;
    }

    public function website($data)
    {
        $websiteCustomerAttribute = $this->_getConfig('customer_attribute_website', 'website');
        $selfData = [
            'title' => 'Website',
            'placeholder' => 'Website',
            'merge' => $data,
            'value' => $this->_getCustomerData($websiteCustomerAttribute)
        ];
        $configuration = $this->_processElement($selfData, __FUNCTION__);
        $html = $this->_render($configuration);
        return $html;
    }

    public function interest($data)
    {
        $selfData = [
            'title' => 'Interests',
            'placeholder' => 'Interests',
            'merge' => $data
        ];
        $configuration = $this->_processElement($selfData, __FUNCTION__);
        $listGroup = $configuration->getListGroup();
        $list = array();
        $childData = '';
        if (is_string($listGroup) && strlen($listGroup)) {
            $list = $this->_getConfig('interests_list_' . $listGroup, array());
        }
        $itemHtml = AWS_Wholesale_Helper_Data::loadExtensionStatic('li_item.html', 'element');
        if (is_array($list) && is_string($itemHtml)) {
            foreach ($list as $index => $item) {
                $configurationN = clone $configuration;
                $configurationN->setData('id', $configurationN->getData('id') . (string)($index + 1));
                $valueRaw = @$item['label'];
                $value = $this->_escapeHtml($valueRaw);
                $valueDefault = $value;
                $configurationN->setData('value', $value);
                $configurationN->setData('default_value', $valueDefault);
                $childData .= PHP_EOL . @AWS_Wholesale_Helper_Data::replaceStringByMask($itemHtml, $configurationN);
                try {
                    unset($configurationN);
                } catch (Exception $exception) {
                }

            }
        }
        $configuration->setData('child_data', $childData);
        $html = $this->_render($configuration);
        return $html;
    }

    public function comment($data)
    {
        $commentCustomerAttribute = $this->_getConfig('customer_attribute_comment', 'comment');
        $selfData = [
            'title' => 'Comments',
            'placeholder' => 'Comments',
            'merge' => $data,
            'value' => $this->_getCustomerData($commentCustomerAttribute)
        ];
        $configuration = $this->_processElement($selfData, __FUNCTION__);
        $html = $this->_render($configuration);
        return $html;
    }

    public function submit($data)
    {
        $selfData = [
            'title' => 'Submit Form',
            'merge' => $data,
            'value' => 'Submit Form'
        ];
        $configuration = $this->_processElement($selfData, __FUNCTION__);
        $html = $this->_render($configuration);
        return $html;
    }

    public function password($data)
    {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            $selfData = [
                'title' => 'Password',
                'merge' => $data,
                'value' => '',
                'placeholder' => 'Password'
            ];
            $configuration = $this->_processElement($selfData, __FUNCTION__);
            $configuration->setData('value', '');
            $html = $this->_render($configuration);
        } else {
            $html = '';
        }
        return $html;
    }

    protected function _generateRecaptchaScript()
    {
        $result = '';
        try {
            $targetLanguage = 'en';
            $currentLanguage = Mage::app()->getLocale()->getLocale()->toString();
            // TODO here may be should be more accurate logic of determining right loacle
            $targetLanguage = substr($currentLanguage, 0, 2);
        } catch (Exception $exception) {
        }
        $query = array(
            'onload' => 'onloadCallback',
            'render' => 'explicit',
            'hl'     => $targetLanguage
        );
        $result = sprintf(
            '<script src="https://www.google.com/recaptcha/api.js?%s" async="async" defer="defer"></script>',
            http_build_query($query)
        );
        return $result;
    }

    public function recaptcha($data)
    {
        $idSuffix = uniqid();
        $childData = $this->_generateRecaptchaScript();
        $selfData = [
            'id' => __FUNCTION__ . $idSuffix,
            'name' => __FUNCTION__ . $idSuffix,
            'merge' => $data,
            'child_data' => $childData,
            'recaptcha_site_key' => $this->_getConfig('recaptcha_site_key', ''),
            'recaptcha_theme' => $this->_getConfig('recaptcha_theme', ''),
            'recaptcha_type' => $this->_getConfig('recaptcha_type', ''),
            'recaptcha_size' => $this->_getConfig('recaptcha_size', ''),
        ];
        $configuration = $this->_processElement($selfData, __FUNCTION__);
        $html = $this->_render($configuration);
        return $html;
    }
}
<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

class AWS_Wholesale_Helper_Data extends Mage_Core_Helper_Abstract
{
    const SEND_EMAIL_ACTION_GUEST_AFTER_REGISTRATION = 'scenario_new_guest_after_reg';

    const ADDITIONAL_CONFIGURATION_CUSTOMER_FILE_ATTRIBUTE = 'customer_attribute_file';
    const ADDITIONAL_CONFIGURATION_CUSTOMER_WEBSITE_ATTRIBUTE = 'customer_attribute_website';
    const ADDITIONAL_CONFIGURATION_CUSTOMER_COMPANY_ATTRIBUTE = 'customer_attribute_businessname';
    const ADDITIONAL_CONFIGURATION_CUSTOMER_COMMENT_ATTRIBUTE = 'customer_attribute_comment';
    const ADDITIONAL_CONFIGURATION_CUSTOMER_INTERESTS_ATTRIBUTE = 'customer_attribute_interests';
    const ADDITIONAL_CONFIGURATION_RECAPTCHA_SITE_KEY = 'recaptcha_site_key';
    const ADDITIONAL_CONFIGURATION_RECAPTCHA_TYPE = 'recaptcha_type';
    const ADDITIONAL_CONFIGURATION_RECAPTCHA_THEME = 'recaptcha_theme';
    const ADDITIONAL_CONFIGURATION_RECAPTCHA_SIZE = 'recaptcha_size';
    const ADDITIONAL_CONFIGURATION_DEBUG_MODE = 'debug_mode';
    const CORE_CONFIG_SALT_PASSWORD = 'salt_password';

    protected static $_encryptSalt = null;
    protected static $_additionalConfigurations = null;
    protected static $_rules = null;
    protected static $_interests = null;
    protected static $_nonCachableBlocks = null;
    protected static $_debugMode = null;

    protected static $_syncMapWasUpdated = false;

    public static function isDebug()
    {
        if (is_null(static::$_debugMode)) {
            try {
                $configs = static::getAdditionalConfigurations();
                if (isset($configs[self::ADDITIONAL_CONFIGURATION_DEBUG_MODE])) {
                    static::$_debugMode = (bool)$configs[self::ADDITIONAL_CONFIGURATION_DEBUG_MODE];
                }
            } catch (Exception $exception) {}
        }
        return static::$_debugMode;
    }

    public static function getNonCachableBlocks()
    {
        if (is_null(static::$_nonCachableBlocks)) {
            try {
                $blocks = static::getWholesaleConfig('non_cachable_blocks');
                static::$_nonCachableBlocks = explode(',', $blocks);
            } catch (Exception $exception) {
            }
        }
        if (!is_array(static::$_nonCachableBlocks)) {
            static::$_nonCachableBlocks = array();
        }
        return static::$_nonCachableBlocks;
    }

    public static function getAdditionalConfigurations()
    {
        if (is_null(static::$_additionalConfigurations)) {
            try {
                $configsRaw = static::getWholesaleConfig('additional_settings');
                $configs = unserialize($configsRaw);
                static::$_additionalConfigurations = array();
                foreach ($configs as $index => $data) {
                    if (isset($data['key']) && isset($data['value']) && is_string($data['value']) && strlen($data['value'])) {
                        static::$_additionalConfigurations[$data['key']] = $data['value'];
                    }
                }
            } catch (Exception $exception) {
            }
        }
        if (!is_array(static::$_additionalConfigurations)) {
            static::$_additionalConfigurations = array();
        }
        return static::$_additionalConfigurations;
    }

    public static function getWholesaleConfigRules()
    {
        if (is_null(static::$_rules)) {
            try {
                $configsRaw = static::getWholesaleConfig('manage_rules');
                static::$_rules = Mage::helper('core/unserializeArray')->unserialize($configsRaw);
            } catch (Exception $exception) {
            }
        }
        if (!is_array(static::$_rules)) {
            static::$_rules = array();
        }
        return static::$_rules;
    }

    public static function getInterestsConfigRules()
    {
        if (is_null(static::$_interests)) {
            try {
                $configsRaw = static::getWholesaleConfig('list', 'aws_wholesale_interests');
                static::$_interests = unserialize($configsRaw);
            } catch (Exception $exception) {
            }
        }
        if (!is_array(static::$_interests)) {
            static::$_interests = array();
        }
        return static::$_interests;
    }

    public static function getInterestsListByGroup($groupName)
    {
        $groupName = @strtolower($groupName);
        $result = array();
        foreach (static::getInterestsConfigRules() as $index => $interestItem) {
            if (is_array($interestItem) && isset($interestItem['group']) && $groupName === @strtolower($interestItem['group'])) {
                $result[] = $interestItem;
            }
        }
        return $result;
    }

    public static function getInterestsGroups(&$groupedItems)
    {
        $groupedItems = array();
        foreach (static::getInterestsConfigRules() as $index => $interestItem) {
            if (isset($interestItem['group']) && isset($interestItem['label'])) {
                if (!is_array($groupedItems[$interestItem['group']])) {
                    $groupedItems[$interestItem['group']] = array();
                }
                $groupedItems[$interestItem['group']][] = $interestItem;
            }
        }
        return array_keys($groupedItems);
    }

    public static function getWholesaleConfig($item, $level = 'general')
    {
        return Mage::getStoreConfig('aws_wholesale/' . $level . '/' . $item);
    }
    public static function getWholesaleConfigSample($part)
    {
        return Mage::getStoreConfig('aws_wholesale/' . $part);
    }

    public static function getWholesaleScenarioConfig($item, $part)
    {
        return Mage::getStoreConfig('aws_wholesale/' . $item .'/' . $item . '_' . $part);
    }

    public static function sendAllEmails($action, $data, $requested = false, $activationChange = false)
    {
        if (!is_array($data) || !isset($data['customer']) || !isset($data['register'])) {
            return ;
        }
        $sendEmailsFlag = explode(',', static::getWholesaleScenarioConfig($action, 'after_reg_email_flag'));
        if ($requested && in_array(AWS_Wholesale_Model_Config_Source_SendEmail::SEND_EMAIL_TO_CLIENT, $sendEmailsFlag)) {
            static::sendEmailClient($action, $data);
        }
        if ($requested && in_array(AWS_Wholesale_Model_Config_Source_SendEmail::SEND_EMAIL_TO_CUSTOMER, $sendEmailsFlag)) {
            static::sendEmailCustomer($action, $data);
        }
        $forceActivationFlag = AWS_Wholesale_Model_Register::$dataTunnel['force_notify_customer'];
        $activationChangeFlag = static::getWholesaleConfigSample('aws_wholesale_activation/aws_wholesale_activation_enabled');
        if (!is_null($forceActivationFlag)) {
            $activationChangeFlag = $forceActivationFlag;
        }
        if (AWS_Wholesale_Model_Config_Source_ActivationState::ACTIVATION_STATE_UNCHANGED != $activationChange && $activationChangeFlag) {
            $suffix = AWS_Wholesale_Model_Config_Source_ActivationState::ACTIVATION_STATE_ENABLED == $activationChange
                ? 'on_email' : 'off_email';
            static::sendEmailCustomer('aws_wholesale_activation', $data, $suffix);
        }
    }

    public static function sendEmailCustomer($action, $data, $suffix = 'after_reg_email')
    {
        $customer = @$data['customer'];
        $emailTemplateId  = static::getWholesaleScenarioConfig($action, $suffix);
        $emailTemplateId = static::tryLoadEmailTemplate($emailTemplateId);
        if (!$emailTemplateId) {
            return false;
        }
        $fromEmail = static::getWholesaleConfig('client_email_identity');
        $recipientEmail = $customer->getEmail();
        $recipientName = $customer->getName();
        $customer['customerName'] = $recipientName;
        return static::sendEmail($fromEmail, $recipientEmail, $recipientName, $emailTemplateId, $data);
    }

    public static function tryLoadEmailTemplate($emailTemplateId)
    {
        if (is_string($emailTemplateId) && !is_numeric($emailTemplateId)) {
            $emailTemplate = Mage::getModel('core/email_template')->loadByCode($emailTemplateId);
            $emailTemplateId = $emailTemplate->getId();
            if (!$emailTemplateId) {
                $emailTemplate = Mage::getModel('core/email_template')->loadDefault($emailTemplateId);
                $emailTemplateId = $emailTemplate->getId();
                if (!$emailTemplateId) {
                    return false;
                }
            }
        }
        return $emailTemplateId;
    }

    public static function sendEmailClient($action, $data)
    {

        $emailTemplateId  = static::getWholesaleScenarioConfig($action, 'after_reg_email_client');
        $emailTemplateId = static::tryLoadEmailTemplate($emailTemplateId);
        if (!$emailTemplateId) {
            return false;
        }
        $fromEmail = static::getWholesaleConfig('client_email_identity');

        $recipientEmailRaw = static::getWholesaleConfig('client_emails');
        $recipientEmail = array_map(
            function($item) {
                return trim($item);
            },
            explode(',', $recipientEmailRaw)
        );
        $recipientName = $recipientEmail;
        return static::sendEmail($fromEmail, $recipientEmail, $recipientName, $emailTemplateId, $data);
    }

    protected static function sendEmail($senderId, $recipientEmail, $recipientName, $templateId, $data)
    {
        $storeId = Mage::app()->getStore()->getId();
        $senderEmail = Mage::getStoreConfig('trans_email/' . $senderId . '/email');
        $senderName = Mage::getStoreConfig('trans_email/' . $senderId . '/name');
        $sender = array(
            'name' => $senderName,
            'email' => $senderEmail
        );
        if (is_object($data)) {
            $data = @$data->getData();
        }
        return Mage::getModel('core/email_template')
            ->sendTransactional($templateId, $sender, $recipientEmail, $recipientName, $data, $storeId);
        // Mage::getSingleton('core/session')->addSuccess(__('We Will Contact You Very Soon.'));
    }

    public static function getCustomerData($customerData = null)
    {
        $customer = new Varien_Object();
        if (!$customerData) {
            $customer = Mage::getSingleton('customer/session')->getCustomer();
        } elseif (is_array($customerData)) {
            $customer = Mage::getModel('customer/customer')->setId(null);
            $customer->addData($customerData);
        } elseif (is_a($customerData, 'Mage_Customer_Model_Customer')) {
            $customer = $customerData;
        }
        return $customer;
    }

    public static function getCustomersGroupArray()
    {
        $result = array(
            'label' => AWS_Wholesale_Model_Register::CUSTOMER_GROUP_NOT_LOGGED_IN,
            'value' => @AWS_Wholesale_Model_Register::getNameByGroupId(AWS_Wholesale_Model_Register::CUSTOMER_GROUP_NOT_LOGGED_IN)
        );
        try {
            $customerGroups = Mage::getResourceModel('customer/group_collection')->load()->toOptionArray();
            $found = false;
            foreach ($customerGroups as $group) {
                if ($group['value'] == 0) {
                    $found = true;
                }
            }
            if (!$found) {
                $result = array_merge_recursive($customerGroups, $result);
            } else {
                $result = $customerGroups;
            }
        } catch (Exception $exception) {
            return $result;
        }
        return $result;
    }

    public static function getWholesaleGroupId($rule)
    {
        $result = false;
        if (is_array($rule) && isset($rule['target_customer_group'])) {
            $result = $rule['target_customer_group'];
        }
        return $result;
    }

    /**
     * @param $customer Mage_Customer_Model_Customer
     * @param bool $savedBillingId
     * @return bool|false|Mage_Core_Model_Abstract|Mage_Customer_Model_Address
     */
    public static function getBillingAddress($customer, $savedBillingId = false)
    {
        $billing = false;
        if (is_a($customer, 'Mage_Customer_Model_Customer')) {
            if ($savedBillingId) {
                try {
                    $billing = Mage::getModel('customer/address')->load($savedBillingId);
                    if ($billing->getId()) {
                        return $billing;
                    }
                } catch (Exception $exception) {}
            }
            $billing = $customer->getPrimaryBillingAddress();
            if (!$billing || !count($billing->getData())) {
                $billing = $customer->getDefaultBillingAddress();
            }
            if (!$billing || !count($billing->getData())) {
                $billingId = $customer->getDefaultBilling();
                $billing = Mage::getModel('customer/address');
                $billing->load($billingId);
            }
        }
        $billing = $billing ? $billing : Mage::getModel('customer/address');
        if (!$billing->getCustomerId() && $customer->getId()) {
            $billing->setCustomer($customer);
        }
        return $billing;
    }

    public static function testCustomerGroupId($id)
    {
        $group = Mage::getModel('customer/group');
        try {
            $group->load($id);
            if ($group->getId() || Mage_Customer_Model_Group::NOT_LOGGED_IN_ID === (int)$id) {
                return true;
            }
        } catch (Exception $exception) {
            ;
        }
        return false;
    }

    public static function saveUploadedFile(
        $name,
        $allowedExtensions = array('jpg', 'jpeg', 'gif', 'png', 'pdf', 'doc', 'docx', 'rtf', 'odt', 'txt', 'csv'),
        $saveBasePath = 'media',
        $saveSubdirectory = 'customer',
        $fileName = 'uploadedFile',
        $returnFileNameOnly = true
    )
    {
        $result = false;
        $uploadSuccessfully = false;
        $fileFullName = '';
        $path = '';
        if (!is_array($_FILES)) {
            return $result;
        }
        $uploadedFileName = @$_FILES[$name]['name'];
        if (isset($uploadedFileName) && strlen($uploadedFileName)) {
            try {
                $uploader = new Varien_File_Uploader($name);
                if (is_array($allowedExtensions) && count($allowedExtensions)) {
                    $uploader->setAllowedExtensions($allowedExtensions);
                }
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(false);
                $path = rtrim(Mage::getBaseDir($saveBasePath) . DS . $saveSubdirectory, DS);
                if (!file_exists($path)) {
                    if (!@mkdir($path, 0777, true)) {
                        return $result;
                    }
                }
                $fileFullName = trim($fileName . '.' . pathinfo($uploadedFileName, PATHINFO_EXTENSION), DS);
                $uploadSuccessfully = $uploader->save($path . DS, $fileFullName);
            } catch (Exception $exception) {
                ;
            } finally {
                if ($uploadSuccessfully) {
                    $result = $returnFileNameOnly ? DS . $fileFullName : $path . DS . $fileFullName;
                }
            }
        }
        return $result;
    }

    public static function renderCmsBlock($cmsBlockId, $variables = array())
    {
        $message = '';
        if (!$cmsBlockId) {
            return $message;
        }
        $cmsBlock = Mage::getModel('cms/block')
            ->setStoreId(Mage::app()->getStore()->getId());
        try {
            $cmsBlock->load($cmsBlockId);
            if ($cmsBlock->getId()) {
                $filterModel = Mage::getModel('cms/template_filter');
                $filterModel->setVariables($variables);
                $message = $filterModel->filter($cmsBlock->getContent());
            }
        } catch (Exception $exception) {
            ;
        }
        return $message;
    }

    public static function loadExtensionStatic($filePath, $group = 'html')
    {
        $buffer = false;
        $path = dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . $group . DIRECTORY_SEPARATOR . $filePath;
        if (file_exists($path)) {
            try {
                $buffer = file_get_contents($path);
            } catch (Exception $exception) {
                Mage::logException($exception);
            }
        }
        return $buffer;
    }

    public static function loadDocFile()
    {
        $buffer = false;
        $path = dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'doc' . DIRECTORY_SEPARATOR . 'readme.md';
        if (file_exists($path)) {
            try {
                $buffer = file_get_contents($path);
            } catch (Exception $exception) {
                Mage::logException($exception);
            }
        }
        return $buffer;
    }

    public static function detectRuleByUrl($rules, $url)
    {
        $result = false;
        if (is_array($rules)) {
            foreach ($rules as $index => $item) {
                if (is_array($item) && isset($item['rule_name']) && isset($item['referrer_url'])) {
                    $ruleUrl = $item['referrer_url'];
                    if (is_string($ruleUrl) && 0 === strpos($ruleUrl, '/')) {
                        if (preg_match($ruleUrl, $url)) {
                            $result = $item;
                            break;
                        }
                    } else {
                        if (false !== strpos($url, $ruleUrl)) {
                            $result = $item;
                            break;
                        }
                    }
                }
            }
        }
        return $result;
    }

    public static function detectRuleByRequest($rules, $params, $decodeFlag = false)
    {
        $result = false;
        if (is_array($params) && isset($params['rule'])) {
            $ruleName = $params['rule'];
        } elseif (is_string($params) && strlen($params)) {
            $ruleName = $params;
        } else {
            return $result;
        }
        if ($decodeFlag) {
            $ruleName = static::decryptData($ruleName);
        }
        foreach ($rules as $index => $item) {
            if (is_array($item) && isset($item['rule_name'])) {
                if ($ruleName === $item['rule_name']) {
                    $result = $item;
                    break;
                }
            }
        }
        return $result;
    }

    public static function replaceStringByMask($string, $array, $quoteOpen = '{{', $quoteClose = '}}')
    {
        // Anonymous functions become available after PHP 5.3
        return preg_replace_callback('/' . $quoteOpen . '(.*?)' . $quoteClose. '/', function ($fields) use ($array) {
            return isset($array[$fields[1]]) ? $array[$fields[1]] : '';
        }, $string);
    }

    public static function getSaltPassword()
    {
        if (is_null(static::$_encryptSalt)) {
            $salt = static::getWholesaleConfig(self::CORE_CONFIG_SALT_PASSWORD);
            if (is_null($salt) || (is_string($salt)) && 2 >= strlen($salt)) {
                $salt = 'aws-wh-r-f-d9';
            }
            static::$_encryptSalt = $salt;
        }
        return static::$_encryptSalt;
    }

    public static function encryptData($data)
    {
        $dataBuffer = @openssl_encrypt($data, 'AES-256-CBC', static::getSaltPassword());
        if (!$dataBuffer) {
            $dataBuffer = Mage::helper('core')->encrypt($data);
        }
        return $dataBuffer;
    }

    public static function decryptData($data)
    {
        $dataBuffer = @openssl_decrypt($data, 'AES-256-CBC', static::getSaltPassword());
        if (!$dataBuffer) {
            $dataBuffer = Mage::helper('core')->decrypt($data);
        }
        return $dataBuffer;
    }

    /**
     * Update sync map for Register - Customer inner fields according to settings in Admin Panel
     */
    public static function updateRegisterModelMaps()
    {
        if (!static::$_syncMapWasUpdated) {
            $updateAttributes = array(
                'file' => self::ADDITIONAL_CONFIGURATION_CUSTOMER_FILE_ATTRIBUTE,
                'comment' => self::ADDITIONAL_CONFIGURATION_CUSTOMER_COMMENT_ATTRIBUTE,
                'website_link' => self::ADDITIONAL_CONFIGURATION_CUSTOMER_WEBSITE_ATTRIBUTE,
                'interests_default' => self::ADDITIONAL_CONFIGURATION_CUSTOMER_INTERESTS_ATTRIBUTE,
            );
            $configs = static::getAdditionalConfigurations();
            foreach ($updateAttributes as $attributeOrigin => $attributeCustom) {
                if (isset($configs[$attributeCustom])) {
                    $attributeValue = $configs[$attributeCustom];
                    if (strlen($attributeValue)) {
                        if (isset(AWS_Wholesale_Model_Register::$importMapParams[$attributeOrigin])) {
                            AWS_Wholesale_Model_Register::$importMapParams[$attributeOrigin] = $attributeValue;
                        }
                    }
                }
            }
            static::$_syncMapWasUpdated = true;
        }
    }

    protected function _redirectReferer($defaultUrl=null)
    {

        $refererUrl = $this->_getRefererUrl();
        if (empty($refererUrl)) {
            $refererUrl = empty($defaultUrl) ? Mage::getBaseUrl() : $defaultUrl;
        }

        $this->getResponse()->setRedirect($refererUrl);
        return $this;
    }

    /**
     * Identify referrer url via all accepted methods (HTTP_REFERER, regular or base64-encoded request param)
     *
     * @return string
     */
    public static function getReferrerUrl()
    {
        $request = Mage::app()->getRequest();
        $referrerUrl = $request->getServer('HTTP_REFERER');
        if ($url = $request->getParam(Mage_Core_Controller_Varien_Action::PARAM_NAME_REFERER_URL)) {
            $referrerUrl = $url;
        }
        if ($url = $request->getParam(Mage_Core_Controller_Varien_Action::PARAM_NAME_BASE64_URL)) {
            $referrerUrl = Mage::helper('core')->urlDecode($url);
        }
        if ($url = $request->getParam(Mage_Core_Controller_Varien_Action::PARAM_NAME_URL_ENCODED)) {
            $referrerUrl = Mage::helper('core')->urlDecode($url);
        }

        if (!static::_isUrlInternal($referrerUrl)) {
            $referrerUrl = Mage::app()->getStore()->getBaseUrl();
        }
        return $referrerUrl;
    }

    /**
     * Check url to be used as internal
     *
     * @param   string $url
     * @return  bool
     */
    protected static function _isUrlInternal($url)
    {
        if (strpos($url, 'http') !== false) {
            /**
             * Url must start from base secure or base unsecure url
             */
            if (0 === (strpos($url, Mage::app()->getStore()->getBaseUrl()))
                || (0 === strpos($url, Mage::app()->getStore()->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK, true)))
            ) {
                return true;
            }
        }
        return false;
    }
}
<?php

/**
 * Absolute Web Services Intellectual Property
 *
 * @copyright    Copyright © 1999-2017 Absolute Web Services, Inc. (http://www.absolutewebservices.com)
 * @author       Absolute Web Services
 * @license      http://www.absolutewebservices.com/license-agreement/  Single domain license
 * @terms of use http://www.absolutewebservices.com/terms-of-use/
 */

class AWS_Wholesale_Helper_LogicHub extends Mage_Core_Helper_Abstract
{
    const LH_C_NOT_REGISTERED = 0;
    const LH_C_REGISTERED = 1;
    const LH_C_REGISTER_BY_CUSTOMER_EMAIL_OR_ID = 2;
    // const LH_C_REGISTER_BY_CUSTOMER_EMAIL = 4;
    const LH_C_USED_EMAIL = 8;
    const LH_C_IS_WHOLESALE = 16;
    const LH_C_EMAIL_MISMATCH = 32;

    // Next "code-uglifying" is needed to make it work on PHP 5.4, as it doesn't support "evaluatable" const / properties
    const LH_R_GUEST_NEW_EMAIL = /*self::LH_C_NOT_REGISTERED*/ 0;
    const LH_R_GUEST_REGED_EMAIL = /*self::LH_C_NOT_REGISTERED | self::LH_C_USED_EMAIL*/ 8;
    const LH_R_CUSTOMER_NEW_REQ = /*self::LH_C_REGISTERED*/ 1;
    const LH_R_CUSTOMER_EXISTS_REQ = /*self::LH_C_REGISTERED | self::LH_C_REGISTER_BY_CUSTOMER_EMAIL_OR_ID*/ 3;
    const LH_R_CUSTOMER_WHOLESALE = /*self::LH_C_REGISTERED | self::LH_C_IS_WHOLESALE*/ 17;
    const LH_R_CUSTOMER_ERROR = /*self::LH_C_REGISTERED | self::LH_C_EMAIL_MISMATCH*/ 33;
    const LH_R_CUSTOMER_ERROR_EXISTS_REQ = /*self::LH_C_REGISTERED | self::LH_C_EMAIL_MISMATCH | self::LH_C_REGISTER_BY_CUSTOMER_EMAIL_OR_ID*/ 35;
    const LH_R_ERROR = -1;

    const LH_E_FIRST_NAME = 1;
    const LH_E_LAST_NAME = 2;
    const LH_E_BUSINESS_NAME = 4;
    const LH_E_ADDRESS_LINE_1 = 8;
    const LH_E_ADDRESS_LINE_2 = 8; //16;
    const LH_E_CITY = 32;
    const LH_E_COUNTRY = 64;
    const LH_E_REGION = 128;
    const LH_E_ZIP = 256;
    const LH_E_PHONE = 512;
    const LH_E_EMAIL = 1024;
    const LH_E_LICENSE = 2048;
    const LH_E_WEBSITE = 4096;
    const LH_E_INTEREST = 8192;
    const LH_E_COMMENT = 16384;
    const LH_E_DISPLAY_REGISTRATION_BUTTON = 32768;

    const LH_A_NOT_ALLOWED_UPDATE_DATA = 0;
    const LH_A_REGISTRATION_WITH_REQUEST = 1;
    const LH_A_UPDATE_WITH_REQUEST = 2;
    const LH_A_UPDATE_DATA_ONLY = 4;

    const DEFAULT_ERROR_REDIRECT = null;

    protected static $_registrationSchema = array();

    public static function init()
    {
        if (!count(static::$_registrationSchema)) {
            static::$_registrationSchema = array(
                self::LH_R_GUEST_NEW_EMAIL => array(
                    'show_elements' => self::LH_E_FIRST_NAME | self::LH_E_LAST_NAME | self::LH_E_BUSINESS_NAME |
                        self::LH_E_ADDRESS_LINE_1 | self::LH_E_ADDRESS_LINE_2 | self::LH_E_CITY |
                        self::LH_E_COUNTRY | self::LH_E_COUNTRY | self::LH_E_REGION |
                        self::LH_E_ZIP | self::LH_E_PHONE | self::LH_E_EMAIL |
                        self::LH_E_LICENSE | self::LH_E_WEBSITE | self::LH_E_INTEREST |
                        self::LH_E_COMMENT | self::LH_E_DISPLAY_REGISTRATION_BUTTON,
                    'allow_registration' => self::LH_A_REGISTRATION_WITH_REQUEST,
                    'on_error_message' => 'Problem occurred while processing next fields: %s',
                    'on_error_redirect' => self::DEFAULT_ERROR_REDIRECT,
                    'on_success_redirect' => 'customer/account/'
                ),
                self::LH_R_GUEST_REGED_EMAIL => array(
                    'show_elements' => self::LH_E_FIRST_NAME | self::LH_E_LAST_NAME | self::LH_E_BUSINESS_NAME |
                        self::LH_E_ADDRESS_LINE_1 | self::LH_E_ADDRESS_LINE_2 | self::LH_E_CITY |
                        self::LH_E_COUNTRY | self::LH_E_COUNTRY | self::LH_E_REGION |
                        self::LH_E_ZIP | self::LH_E_PHONE | self::LH_E_EMAIL |
                        self::LH_E_LICENSE | self::LH_E_WEBSITE | self::LH_E_INTEREST |
                        self::LH_E_COMMENT | self::LH_E_DISPLAY_REGISTRATION_BUTTON,
                    'allow_registration' => self::LH_A_NOT_ALLOWED_UPDATE_DATA,
                    // 'on_error_message' => 'This email is already in use, try another email or login',
                    'on_error_message_cms_block' => 'scenario_guest_used_email/scenario_guest_used_email_warning_show',
                    'on_error_redirect' => self::DEFAULT_ERROR_REDIRECT
                ),
                self::LH_R_CUSTOMER_NEW_REQ => array(
                    'show_elements' => self::LH_E_FIRST_NAME | self::LH_E_LAST_NAME | self::LH_E_BUSINESS_NAME |
                        self::LH_E_ADDRESS_LINE_1 | self::LH_E_ADDRESS_LINE_2 | self::LH_E_CITY |
                        self::LH_E_COUNTRY | self::LH_E_COUNTRY | self::LH_E_REGION |
                        self::LH_E_ZIP | self::LH_E_PHONE |
                        self::LH_E_LICENSE | self::LH_E_WEBSITE | self::LH_E_INTEREST |
                        self::LH_E_COMMENT | self::LH_E_DISPLAY_REGISTRATION_BUTTON | self::LH_E_DISPLAY_REGISTRATION_BUTTON,
                    'allow_registration' => self::LH_A_UPDATE_WITH_REQUEST,
                    'on_error_message' => 'Problem occurred while processing next fields: %s',
                    'on_error_redirect' => self::DEFAULT_ERROR_REDIRECT,
                    'on_success_redirect' => 'customer/account/'
                ),
                self::LH_R_CUSTOMER_EXISTS_REQ => array(
                    'show_elements' => 0,
                    'allow_registration' => self::LH_A_UPDATE_DATA_ONLY,
                    'on_error_message' => 'You have already submitted registration form early, please contact us for details.',
                    'on_error_redirect' => self::DEFAULT_ERROR_REDIRECT
                ),
                self::LH_R_CUSTOMER_WHOLESALE => array(
                    'show_elements' => 0,
                    'allow_registration' => self::LH_A_UPDATE_DATA_ONLY,
                    'on_error_message' => 'You are already registered customer, congratulations!',
                    'on_error_message_cms_block' => 'scenario_wholesale/scenario_wholesale_warning_show',
                    'on_error_redirect' => self::DEFAULT_ERROR_REDIRECT
                ),
                self::LH_R_ERROR => array(
                    'show_elements' => 0,
                    'allow_registration' => self::LH_A_NOT_ALLOWED_UPDATE_DATA,
                    'on_error_message' => 'Problem occurred while processing next fields: %s',
                    'on_error_redirect' => self::DEFAULT_ERROR_REDIRECT
                )
            );
        }
    }

    /**
     * @param $data
     * @param $session
     * @param $customer Mage_Customer_Model_Customer
     * @param $register
     * @param $rule
     * @param bool $showFirstly
     * @return int
     */
    public static function determineConditions($data, $session, $customer, $register, $rule, $showFirstly = false)
    {
        static::init();
        // 1. preparation
        $conditionsFlag = 0;
        if (is_null($data->getData('show_schema')) && !$showFirstly) {
            return self::LH_R_ERROR;
        }
        $newEmail = trim($data->getEmail());
        // 2. checking
        // 2.1 registered customer
        if ($session->isLoggedIn()) {
            $conditionsFlag = $conditionsFlag | static::LH_C_REGISTERED;
            // wholesale already!
            if ($customer->getGroupId() == AWS_Wholesale_Helper_Data::getWholesaleGroupId($rule)) {
                return $conditionsFlag = static::LH_C_REGISTERED | static::LH_C_IS_WHOLESALE;
            }
            // 2.1.1 if we already have request for wholesale registration
            $register->loadByCustomerId($customer->getId());
            // ... try find request
            if (!$register->getId()) {
                $register->loadByEmail($customer->getEmail());
            }
            if ($register->getId()) {
                // ... if request was found
                $conditionsFlag = $conditionsFlag | static::LH_C_REGISTER_BY_CUSTOMER_EMAIL_OR_ID;
                // 2.1.2 if email is not the same as for existed account
                if ($newEmail && $newEmail != $customer->getEmail()) {
                    $conditionsFlag = $conditionsFlag | static::LH_C_EMAIL_MISMATCH;
                }
            }
        } else {
            // 2.2 not registered customer
            if ($newEmail) {
                // 2.2.1 check email usage
                $bufferCustomer = Mage::getModel('customer/customer');
                $bufferCustomer->setData('website_id', Mage::app()->getWebsite()->getId());
                $bufferCustomer->loadByEmail($newEmail);
                if ($bufferCustomer->getId()) {
                    $conditionsFlag = $conditionsFlag | static::LH_C_USED_EMAIL;
                }
            }
        }
        return $conditionsFlag;
    }

    public static function figureOutScenario($conditions)
    {
        static::init();
        if (isset(static::$_registrationSchema[$conditions])) {
            return static::$_registrationSchema[$conditions];
        } else {
            return static::$_registrationSchema[self::LH_R_ERROR];
        }
    }

    public static function getExpressScenario($rule = '', $showFirstly = false)
    {
        static::init();
        $data = new Varien_Object(Mage::app()->getRequest()->getParams());
        $session = Mage::getSingleton('customer/session');
        $customer = $session->getCustomer();
        $register = Mage::getModel('aws_wholesale/register');
        $conditions = static::determineConditions($data, $session, $customer, $register, $rule, $showFirstly);
        return static::figureOutScenario($conditions);
    }

    public static function correctScenarioToCustomersData($customer, $scenario)
    {
        static::init();
        return $scenario;
    }

    public static function getCurrentCustomerData()
    {
        $session = @Mage::getSingleton('customer/session');
        return @$session->getCustomer();
    }

    public static function gdi($alias, $scenarioShowSchema)
    {
        static::init();
        $result = false;
        switch ($alias) {
            case 'firstname':
            case 'fn':
                $result = self::LH_E_FIRST_NAME;
                break;
            case 'lastname':
            case 'ln':
                $result = self::LH_E_LAST_NAME;
                break;
            case 'businessname':
            case 'business_name':
            case 'company':
            case 'bn':
                $result = self::LH_E_BUSINESS_NAME;
                break;
            case 'street':
            case 'al1':
                $result = self::LH_E_ADDRESS_LINE_1;
                break;
            case 'aln':
                $result = self::LH_E_ADDRESS_LINE_2;
                break;
            case 'city':
                $result = self::LH_E_CITY;
                break;
            case 'country_id':
            case 'country':
                $result = self::LH_E_COUNTRY;
                break;
            case 'region':
            case 'region_id':
                $result = self::LH_E_REGION;
                break;
            case 'postcode':
            case 'zip':
                $result = self::LH_E_ZIP;
                break;
            case 'telephone':
            case 'phone':
                $result = self::LH_E_PHONE;
                break;
            case 'email':
                $result = self::LH_E_EMAIL;
                break;
            case 'file':
            case 'license':
            case 'lic':
                $result = self::LH_E_LICENSE;
                break;
            case 'site':
            case 'website':
            case 'web':
                $result = self::LH_E_WEBSITE;
                break;
            case 'interest':
            case 'interests':
                $result = self::LH_E_INTEREST;
                break;
            case 'comment':
                $result = self::LH_E_COMMENT;
                break;
            case 'password':
            case 'confirmation':
            case 'pwd':
            case 'submit':
            case 'reg-btn':
                $result = self::LH_E_DISPLAY_REGISTRATION_BUTTON;
                break;
        }
        if ($result) {
            return ($scenarioShowSchema & $result) == $result;
        } else {
            return false;
        }
    }

    public static function getRequiredBlankData($requestData = array())
    {
        $result = array(
            'firstname' => true,
            'lastname' => true,
            'company' => true,
            'street' => true,
            'city' => true,
            'country_id' => true,
            'region' => true,
            'region_id' => false,
            'postcode' => true,
            'telephone' => true,
            'email' => true,
            // 'license' => false,
            'website' => false,
            'interest' => true,
            'comment' => false
            // 'show_schema' => ''
        );
        if (is_array($requestData) && isset($requestData['rf']) && is_string($requestData['rf'])) {
            $requiredByRequestRaw = $requestData['rf'];
            $requiredByRequestRaw = AWS_Wholesale_Helper_Data::decryptData($requiredByRequestRaw);
            $requiredByRequest = @explode(',', $requiredByRequestRaw);
            if (is_array($requiredByRequest)) {
                foreach ($requiredByRequest as $required) {
                    if (!isset($result[$required])) {
                        $result[$required] = true;
                    }
                }
                foreach ($result as $field => &$requiredFlag) {
                    if (in_array($field, $requiredByRequest)) {
                        $requiredFlag = true;
                    } else {
                        $requiredFlag = false;
                    }
                }

            }
        }
        return $result;
    }

    protected static function registerParamValue($key, $value)
    {
        try {
            Mage::app()->getRequest()->setParam($key, $value);
            $_POST[$key] = $value;
        } catch (Exception $exception) {
            return false;
        }
        return true;
    }

    protected static function replaceFields(&$data, $requestData)
    {
        // region check
        $first = $requestData->getData('region_id');
        $second = $requestData->getData('region');
        if ($first && 0 < (int)$first) {
            $region = Mage::getModel('directory/region')->load($first);
            $value = $region->getName();
            $data['region'] = $value;
            static::registerParamValue('region', $value);
        } elseif ($second && strlen($second)) {
            $data['region'] = $second;
            $region = Mage::getModel('directory/region')->load('name', $second);
            if ($region->getId()) {
                $data['region_id'] = $region->getId();
                static::registerParamValue('region_id', $region->getId());
            }
        }
        // country check
        $first = $requestData->getData('country');
        $second = $requestData->getData('country_id');
        if ($first && 0 < strlen($first)) {
            $countryId = '';
            $countryCollection = Mage::getModel('directory/country')->getCollection();
            foreach ($countryCollection as $country) {
                if (strtolower($first) === strtolower($country->getName())) {
                    $countryId = $country->getCountryId();
                    break;
                }
            }
            $countryCollection = null;
            if ($countryId) {
                $data['country_id'] = $countryId;
                static::registerParamValue('country_id', $countryId);
            }
        } elseif ($second && 0 < strlen($second)) {
            $country = Mage::getModel('directory/country')->loadByCode($second);
            if ($country->getId()) {
                $data['country'] = $country->getName();
                static::registerParamValue('country', $country->getName());
            }
        }
    }

    protected static function processField($required, &$data, &$errors, $showSchema, $field, $requestData, $customer)
    {
        static::init();
        $requestedField = $requestData->getData($field);
        if (static::gdi($field, $showSchema) && (strlen($requestedField) || is_array($requestedField))) {
            $data[$field] = $requestedField;
        } else {
            if (isset($data[$field]) && (strlen($data[$field]) || (is_array($data[$field])))) {
                return false;
            }
            if (!$customer->getData($field) && true === $required[$field]) {
                $errors[] = $field;
            } else {
                return $field;
            }
        }
        return false;
    }

    public static function validateWholesaleData($requestData, &$data, $scenario, $customer)
    {
        static::init();
        $required = static::getRequiredBlankData($requestData->getData());
        $data = array();
        foreach ($required as $fieldName => $fieldRequired) {
            $data[$fieldName] = '';
        }
        $showSchema = $requestData->getData('show_schema') & $scenario['show_elements'];
        $errors = array();
        $deleteItems = array();
        static::replaceFields($data, $requestData);
        foreach ($data as $item => $value) {
            $flag = static::processField($required, $data, $errors, $showSchema, $item, $requestData, $customer);
            if ($flag) {
                $deleteItems[] = $flag;
            }
        }
        foreach ($deleteItems as $deleteItem) {
            unset($data[$deleteItem]);
        }
        static::postPlaceFields($requestData, $data, $errors);
        return $errors;
    }

    public static function postPlaceFields($requestData, &$data, $errors)
    {
        // replace otherfake with interest
        $flag1 = isset($requestData['otherfake']) && strlen(@$requestData['otherfake']);
        $flag2 = isset($data['interest']) && 0 < count(@$data['interest']) && in_array('on', @$data['interest']);
        if ($flag1 && $flag2) {
            $data['interest'][] = @$requestData['otherfake'];
        }
        if ($flag2) {
            $index = array_search('on', @$data['interest']);
            if (false !== $index) {
                unset($data['interest'][$index]);
            }
        }
        // implode interests into one string
        if (isset($data['interest'])) {
            @$data['interest'] = @implode("\n", $data['interest']);
        }
        if (!count($errors) && !isset($requestData['password'])) {
            $password = Mage::helper('core')->getRandomString(7);
            $requestData['password'] = $password;
            $requestData['confirmation'] = $password;
            if (static::registerParamValue('password', $password)) {
                if (static::registerParamValue('confirmation', $password)) {
                    AWS_Wholesale_Model_Register::$dataTunnel['password_auto_generated'] = $password;
                }
            }
        }
    }

    public static function prepareErrorMessage($scenario, $validationResult)
    {
        $message = '';
        if (!is_array($validationResult)) {
            $validationResult = array($validationResult);
        }
        $cmsBlockId = AWS_Wholesale_Helper_Data::getWholesaleConfigSample(@$scenario['on_error_message_cms_block']);
        if (@$scenario['on_error_message_cms_block'] && $cmsBlockId) {
            /** @var Mage_Cms_Model_Block $cmsBlock */
            $cmsBlock = Mage::getModel('cms/block')
                ->setStoreId(Mage::app()->getStore()->getId());
            try {
                $cmsBlock->load($cmsBlockId);
                if ($cmsBlock->getId()) {
                    $filterModel = Mage::getModel('cms/template_filter');
                    $filterModel->setVariables($validationResult);
                    $message = $filterModel->filter($cmsBlock->getContent());
                }
            } catch (Exception $exception) {
                ;
            }
        }
        return strlen($message) ? $message : sprintf(
            @$scenario['on_error_message'],
            implode(', ', $validationResult)
        );
    }

    public static function prepareException($scenario, $validationResult)
    {
        $exception = null;
        $message = static::prepareErrorMessage($scenario, $validationResult);
        if ($message) {
            $exception = new Exception($message);
        }
        return $exception;
    }

    public static function checkOpenRequest(&$customer, &$register, &$scenario)
    {
        static::init();
        $result = false;
        if (!is_a($customer, 'Mage_Customer_Model_Customer')) {
            return $result;
        }
        if (!is_a($register, 'AWS_Wholesale_Model_Register')) {
            $register = Mage::getModel('aws_wholesale/register');
        }
        if ($register->getCustomerId() != $customer->getId()) {
            $register->loadByCustomerId($customer->getId());
            if (!$register->getId()) {
                $register->loadByEmail($customer->getEmail());
            }
        }
        if ($register->getId()) {
            if (AWS_Wholesale_Model_Config_Source_ActivationState::ACTIVATION_STATE_ENABLED == $register->getActivationState()) {
                $scenario = AWS_Wholesale_Helper_LogicHub::figureOutScenario(AWS_Wholesale_Helper_LogicHub::LH_R_CUSTOMER_WHOLESALE);
            } elseif (AWS_Wholesale_Model_Config_Source_ActivationState::ACTIVATION_STATE_ENABLED != $register->getActivationState()) {
                $scenario = AWS_Wholesale_Helper_LogicHub::figureOutScenario(AWS_Wholesale_Helper_LogicHub::LH_R_CUSTOMER_EXISTS_REQ);
            }
            AWS_Wholesale_Model_Register::$dataTunnel['register'] = $register;
            AWS_Wholesale_Model_Register::$dataTunnel['scenario'] = $scenario;
            $result = true;
        }
        return $result;
    }

    public static function setWholesaleSpecificData($dataSave, $customer)
    {
        // save license
        $hash = Mage::helper('core')->getRandomString($length = 7);
        $date = date('Y-m-d_H-i-s');
        $fileName = AWS_Wholesale_Helper_Data::saveUploadedFile(
            'file', null, 'media',
            'customer',
            'pending-registration-customer-file-' . $date . '-hash-' . $hash,
            true
        );
        if ($fileName && strlen($fileName)) {
            AWS_Wholesale_Model_Register::$dataTunnel['save_file'] = $fileName;
        }
        AWS_Wholesale_Model_Register::$dataTunnel['save_billing'] = true;
        AWS_Wholesale_Model_Register::$dataTunnel['save_other_fields'] = true;
    }

    public static function updateCustomerBillingAddress($customer, $dataSave)
    {
        if (!@AWS_Wholesale_Model_Register::$dataTunnel['save_billing']) {
            return ;
        }
        if (is_a($dataSave, 'Varien_Object')) {
            $dataSave = $dataSave->getData();
        }
        $billing = AWS_Wholesale_Helper_Data::getBillingAddress($customer);
        $daveSaveFiltered = array_intersect_key($dataSave, array_flip(AWS_Wholesale_Model_Register::$importMapAddress));
        $savedBillingId = false;
        // force save billing address
        if ($billing && /*$billing->getId() &&*/ $daveSaveFiltered && count($daveSaveFiltered)) {
            try {
                AWS_Wholesale_Helper_LogicHub::fillDataByArray($billing, $daveSaveFiltered);
                $billing->save();
                $savedBillingId = $billing->getId();

            } catch (Exception $exception) {
                ;
            } finally {
                @AWS_Wholesale_Model_Register::$dataTunnel['save_billing'] = false;
            }
        }
        return $savedBillingId;
    }

    public static function fillDataByArray($object, $array)
    {
        if (!is_a($object, 'Varien_object') || !is_array($array)) {
            return ;
        }
        foreach ($array as $key => $value) {
            $object->setData($key, $value);
        }
    }

    public static function saveUpdatedCustomerData($customer)
    {
        if (!$customer || !is_a($customer, 'Mage_Customer_Model_Customer')) {
            return ;
        }
        $fileLicense = AWS_Wholesale_Model_Register::$dataTunnel['save_file'];
        if ($customer && $fileLicense && is_string($fileLicense) && 3 < strlen($fileLicense)) {
            $configurations = AWS_Wholesale_Helper_Data::getAdditionalConfigurations();
            if (isset($configurations[AWS_Wholesale_Helper_Data::ADDITIONAL_CONFIGURATION_CUSTOMER_FILE_ATTRIBUTE])) {
                $customer->setData($configurations[AWS_Wholesale_Helper_Data::ADDITIONAL_CONFIGURATION_CUSTOMER_FILE_ATTRIBUTE], $fileLicense);
                if (AWS_Wholesale_Helper_Data::isDebug()) {
                    Mage::log('Apply file `' . $fileLicense . '` and saving it for customer ' . $customer->getEmail(), null, 'aws-pending-registration-files-saved.log');
                }
            }
            AWS_Wholesale_Model_Register::$dataTunnel['save_file'] = false;
        }
        $saveOtherFields = AWS_Wholesale_Model_Register::$dataTunnel['save_other_fields'];
        if ($saveOtherFields) {
            try {
                $dataSave = AWS_Wholesale_Model_Register::$dataTunnel['data'];
                $daveSaveFiltered = array_diff_key($dataSave, array_flip(AWS_Wholesale_Model_Register::$importMapAddress));
                AWS_Wholesale_Helper_LogicHub::fillDataByArray($customer, $daveSaveFiltered);
            } catch (Exception $exception) {
                ;
            } finally {
                AWS_Wholesale_Model_Register::$dataTunnel['save_other_fields'] = false;
            }
        }
    }
}